package model

import java.time.{LocalDate, Instant}
import tools.DbProfile._
import dbProfile.simple._
import model.RoyaltyBills.FormType.FormType
import scala.slick.model.ForeignKeyAction.Cascade

/**
 * Created by vnechaev on 10.05.2014.
 */
object RoyaltyBills {
  case class RoyaltyBill(idOpt: Option[Int], orgUnitId: Int, created: Instant,
                         periodStart: LocalDate, periodEnd: LocalDate,
                         totalSales: BigDecimal, royaltySum: BigDecimal, paid: BigDecimal)
  class RoyaltyBillsTable(tag: Tag) extends Table[RoyaltyBill](tag, "royalty_bills") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def orgUnitId = column[Int]("ou")
    def created = column[Instant]("created")
    def periodStart = column[LocalDate]("periodStart")
    def periodEnd = column[LocalDate]("periodEnd")
    def totalSales = column[BigDecimal]("totalSales", types.Money)
    def royaltySum = column[BigDecimal]("royalty", types.Money)
    def paid = column[BigDecimal]("paid", types.Money)
    def * = (id.?, orgUnitId, created, periodStart, periodEnd, totalSales, royaltySum, paid) <> (RoyaltyBill.tupled, RoyaltyBill.unapply)
    def orgUnit = foreignKey("royalty_bills_ou_fk", orgUnitId, OrgUnits.orgUnits)(_.id)
    def periodIdx = index("royalty_bills_period_idx", (orgUnitId, periodStart, periodEnd), unique = true)
  }
  val royaltyBills = TableQuery[RoyaltyBillsTable]
  object FormType extends Enumeration {
    type FormType = Value
    val Inquiry = Value("inquiry")
    val Bill = Value("bill")
    val Act = Value("act")
    val Invoice = Value("invoice")
  }
  implicit val formTypeColumnType = MappedColumnType.base[FormType, String] (
    { ft => ft.toString },
    { s => FormType.withName(s) }
  )
  case class RoyaltyBillForm(billId: Int, formType: FormType, formData: Array[Byte])
  class RoyaltyBillFormsTable(tag: Tag) extends Table[RoyaltyBillForm](tag, "royalty_bill_forms") {
    def billId = column[Int]("billId")
    def formType = column[FormType]("formType", O.Length(10, varying = true))
    def formData = column[Array[Byte]]("formData")
    def * = (billId, formType, formData) <> (RoyaltyBillForm.tupled, RoyaltyBillForm.unapply)
    def bill = foreignKey("royalty_bill_forms_bill_fk", billId, royaltyBills)(_.id, Cascade, Cascade)
    def idx = index("royalty_bill_forms_idx", (billId, formType), unique = true)

  }
  val royaltyBillForms = TableQuery[RoyaltyBillFormsTable]

}
