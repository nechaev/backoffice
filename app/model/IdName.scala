package model

/**
 * Created by sgl on 03.04.14.
 */
trait IdName {
  val id: Int
  val name: String
}

abstract class IdNameEntity extends IdName {
  val idOpt: Option[Int]
  lazy val id = idOpt.get
}
