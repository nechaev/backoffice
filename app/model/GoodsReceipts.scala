package model

import java.time.{Instant, LocalDate}
import tools.DbProfile._
import dbProfile.simple._
import scala.slick.model.ForeignKeyAction.Cascade
import model.DocumentStatus._

/**
 * Created by vnechaev on 10.05.2014.
 */
object GoodsReceipts {
  case class GoodsReceipt(idOpt: Option[Int], created: Instant, orgUnitId: Int, supplierId: Int, sum: BigDecimal,
                          waybillNumber: Option[String], waybillDate: Option[LocalDate],
                          invoiceNumber: Option[String], invoiceDate: Option[LocalDate],
                          comment: String, status: DocumentStatus, postedNumber: Option[String])

  class GoodsReceiptsTable(tag: Tag) extends Table[GoodsReceipt](tag, "goods_receipts") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def created = column[Instant]("created")
    def orgUnitId = column[Int]("ou")
    def supplierId = column[Int]("supplier")
    def sum = column[BigDecimal]("sum", types.Money)
    def waybillNumber = column[Option[String]]("wbNumber", types.DocumentNumber)
    def waybillDate = column[Option[LocalDate]]("wbDate")
    def invoiceNumber = column[Option[String]]("invNumber", types.DocumentNumber)
    def invoiceDate = column[Option[LocalDate]]("invDate")
    def comment = column[String]("comment", types.Comment)
    def status = column[DocumentStatus]("status", types.DocumentStatus)
    def postedNumber = column[Option[String]]("postedNumber", types.DocumentNumber)
    def * = (id.?, created, orgUnitId, supplierId, sum, waybillNumber, waybillDate,
      invoiceNumber, invoiceDate, comment, status, postedNumber) <> (GoodsReceipt.tupled, GoodsReceipt.unapply)
    def orgUnit = foreignKey("goods_receipts_ou_fk", orgUnitId, OrgUnits.orgUnits)(_.id)
    def supplier = foreignKey("goods_receipts_supplier_fk", supplierId, OrgUnits.orgUnits)(_.id)
  }
  val goodsReceipts = TableQuery[GoodsReceiptsTable]
  def apply(id: Int) = goodsReceipts.filter(_.id === id)

  case class GoodsReceiptItem(receiptId: Int, itemId: Int, price: BigDecimal, quantity: BigDecimal)
  class GoodsReceiptsItemsTable(tag: Tag) extends Table[GoodsReceiptItem](tag, "goods_receipts_items") {
    def receiptId = column[Int]("rcpt_id")
    def itemId = column[Int]("item")
    def price = column[BigDecimal]("price", types.Money)
    def quantity = column[BigDecimal]("quantity", types.Quantity)
    def * = (receiptId, itemId, price, quantity) <> (GoodsReceiptItem.tupled, GoodsReceiptItem.unapply)
    def receipt = foreignKey("goods_receipts_items_rcpt_fk", receiptId, goodsReceipts)(_.id, Cascade, Cascade)
    def item = foreignKey("goods_receipts_items_item_fk", itemId, Items.items)(_.id)
    def receiptItemPk = primaryKey("goods_receipts_items_rcpt_pk", (receiptId, itemId))
  }
  val goodsReceiptsItems = TableQuery[GoodsReceiptsItemsTable]

}
