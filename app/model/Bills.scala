package model
import tools.DbProfile._
import dbProfile.simple._
import java.time.LocalDate
import scala.slick.model.ForeignKeyAction.Cascade

/**
 * Created by vnechaev on 26.06.2014.
 */
object Bills {
  case class Bill(idOpt: Option[Int], date: LocalDate, number: String, orgUnitId: Int, supplierId: Int,
                  sum: BigDecimal, paid: BigDecimal, orderId: Option[Int], comment: String)
  class BillTable(tag: Tag) extends Table[Bill](tag, "bills") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def date = column[LocalDate]("date")
    def number = column[String]("number", O.Length(11, varying = true))
    def orgUnitId = column[Int]("orgUnit")
    def supplierId = column[Int]("supplier")
    def sum = column[BigDecimal]("sum", types.Money)
    def paid = column[BigDecimal]("paid", types.Money)
    def orderId = column[Int]("orderId", O.Nullable)
    def comment = column[String]("comment")
    def * = (id.?, date, number, orgUnitId, supplierId, sum, paid, orderId.?, comment) <> (Bill.tupled, Bill.unapply)
    def orgUnit = foreignKey("bills_orgUnit_fk", orgUnitId, OrgUnits.orgUnits)(_.id)
    def supplier = foreignKey("bills_supplier_fk", supplierId, OrgUnits.orgUnits)(_.id)
    def order = foreignKey("bills_order_fk", orderId, CustomerOrders.orders)(_.id)
  }
  val bills = TableQuery[BillTable]

  class BillFormTable(tag: Tag) extends Table[(Int, Array[Byte])](tag, "bill_forms") {
    def billId = column[Int]("billId")
    def formData = column[Array[Byte]]("formData")
    def * = (billId, formData)
    def bill = foreignKey("bill_forms_fk", billId, bills)(_.id, Cascade, Cascade)
    def idx = index("bill_forms_idx", billId, unique = true)
  }
  val billForms = TableQuery[BillFormTable]
}
