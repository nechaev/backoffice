package model

/**
 * Created by sgl on 14.04.14.
 */

sealed class Role(val id: String, val name: String)

object Role {
  case object Admin extends Role("admin", "Администрирование")
  case object Customer extends Role("customer", "Заказы")
  case object Warehouse extends Role("warehouse", "Склад")
  case object Franchise extends Role("franchise", "Франшиза")

  private val roleMap = Map(Seq(
    Admin,
    Customer,
    Warehouse,
    Franchise
  ).map(role => role.id -> role): _*)
  def apply(id: String) = roleMap(id)
  def all = roleMap.values
}
