package model

/**
 * Created by sgl on 31.03.14.
 */
case class ItemGroup(id: Int, name: String, childGroups: List[ItemGroup], items: List[Item])