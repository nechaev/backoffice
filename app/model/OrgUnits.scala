package model

import scala.slick.model.ForeignKeyAction.Cascade
import tools.DbProfile._
import dbProfile.simple._

/**
 * Created by sgl on 01.04.14.
 */
object OrgUnits {
  case class OrgUnit(idOpt: Option[Int], name: String, posCount: Int) extends IdNameEntity

  class OrgUnitsTable(tag: Tag) extends Table[OrgUnit](tag, "org_units") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name", O.Length(100, varying = true))
    def posCount = column[Int]("posCount")
    def * = (id.?, name, posCount) <> (OrgUnit.tupled, OrgUnit.unapply)
    def nameIdx = index("org_units_name_idx", name, unique = true)
  }
  val orgUnits = TableQuery[OrgUnitsTable]
  def apply(id: Int) = orgUnits.filter(_.id === id)

  class UserOrgUnitsTable(tag: Tag) extends Table[(Int, Int)](tag, "org_users") {
    def userId = column[Int]("userId")
    def orgUnitId = column[Int]("ou")
    def * = (userId, orgUnitId)
    def pk = primaryKey("org_users_pk", (userId, orgUnitId))
    def user = foreignKey("org_users_user_fk", userId, Users.users)(_.id, Cascade, Cascade)
    def orgUnit = foreignKey("org_users_ou_fk", orgUnitId, orgUnits)(_.id, Cascade, Cascade)
  }
  val userOrgUnits = TableQuery[UserOrgUnitsTable]
  def userOrgUnitsForUser(userId: Int) = userOrgUnits.filter(_.userId === userId)
  def orgUnitsForUser(userId: Int) = for {
    userOrgUnit <- userOrgUnitsForUser(userId)
    orgUnit <- userOrgUnit.orgUnit
  } yield orgUnit

}
