package model

/**
 * Created by sgl on 28.03.14.
 */
case class Item(
  idOpt: Option[Int],
  syncCode: Option[String],
  name: String,
  uom: String,
  weight: BigDecimal,
  volume: BigDecimal,
  price: BigDecimal
) extends IdNameEntity
