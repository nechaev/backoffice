package model

import java.time.LocalDate
import tools.DbProfile._
import dbProfile.simple._

/**
 * Created by sgl on 02.09.14.
 */
case class ReportingPeriod(number: Int, year: Int, start: LocalDate, end: LocalDate)

object ReportingPeriods {
  class ReportingPeriodTable(tag: Tag) extends Table[ReportingPeriod](tag, "reporting_periods") {
    def number = column[Int]("number")
    def year = column[Int]("year")
    def start = column[LocalDate]("start")
    def end = column[LocalDate]("end")
    def * = (number, year, start, end) <> (ReportingPeriod.tupled, ReportingPeriod.unapply)
    def pk = primaryKey("reporting_periods_pk", (number, year, start, end))
  }
  val reportingPeriods = TableQuery[ReportingPeriodTable]

}
