package model

import scala.slick.model.ForeignKeyAction.Cascade
import scala.slick.jdbc.{GetResult, StaticQuery}
import StaticQuery.interpolation
import tools.DbProfile._
import dbProfile.simple._

/**
 * Created by sgl on 31.03.14.
 */
object Items {
  class ItemsTable(tag: Tag) extends Table[Item](tag, "items") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def syncCode = column[String]("syncCode", O.Length(11, varying = true))
    def name = column[String]("name", O.Length(100, varying = true))
    def uom = column[String]("uom", O.Length(30, varying = true))
    def weight = column[BigDecimal]("weight", types.Weight)
    def volume = column[BigDecimal]("volume", types.Volume)
    def price = column[BigDecimal]("price", types.Money)
    def * = (id.?, syncCode.?, name, uom, weight, volume, price) <> (Item.tupled, Item.unapply)
    def nameIdx = index("items_name_idx", name)
    def syncCodeIdx = index("items_syncCode_idx", syncCode)
  }
  val items = TableQuery[ItemsTable]
  def apply(id: Int) = items.filter(_.id === id)

  case class ItemGroup(idOpt: Option[Int], name: String, parentId: Option[Int]) extends IdNameEntity
  class ItemGroupsTable(tag: Tag) extends Table[ItemGroup](tag, "item_groups") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name", O.Length(100, varying = true))
    def parentId = column[Option[Int]]("parent")
    def * = (id.?, name, parentId) <> (ItemGroup.tupled, ItemGroup.unapply)
    def parent = foreignKey("item_groups_parent_fk", parentId, itemGroups)(_.id, Cascade, Cascade)
    def nameIdx = index("item_groups_name_idx", (parentId, name), unique = true)
  }
  val itemGroups = TableQuery[ItemGroupsTable]

  class ItemGroupItemsTable(tag: Tag) extends Table[(Int, Int)](tag, "item_group_items") {
    def groupId = column[Int]("groupId")
    def itemId = column[Int]("itemId")
    def * = (groupId, itemId)
    def pk = primaryKey("item_group_items_pk", (groupId, itemId))
    def group = foreignKey("item_group_items_group_fk", groupId, itemGroups)(_.id, Cascade, Cascade)
    def item = foreignKey("item_group_items_item_fk", itemId, items)(_.id, Cascade, Cascade)
  }
  val itemGroupItems = TableQuery[ItemGroupItemsTable]

  def groupItems(groupId: Int) = for {
    gi <- itemGroupItems.filter(_.groupId === groupId)
    item <- gi.item
  } yield item
  def childGroups(groupId: Int) = itemGroups.filter(_.parentId === groupId)

  implicit def getIdNameResult = GetResult(row => ItemGroup(row.<<, row.<<, None))
  def getGroupPath(groupId: Int, rootId: Int)(implicit sess: Session): Seq[IdName] = {
    sql"""
      WITH RECURSIVE g(id, name, parent) AS (
      SELECT id, name, parent FROM item_groups where id = $groupId
      UNION ALL
      SELECT ig.id, ig.name, ig.parent FROM item_groups ig INNER JOIN g ON ig.id = g.parent
      WHERE NOT g.id = $rootId
      ) SELECT * FROM g
    """.as[IdName].list.reverse
  }

  /* //Retrieve all groups and items by single recursive query
  private val itemTreeSql = SQL(
    """SELECT t.group_id, t.group_name, t.parent_id, i.id item_id, i.name item_name, i.uom, i.weight, i.volume
      |FROM (
        |WITH RECURSIVE group_tree (group_id, group_name, parent_id) AS (
        |SELECT g.id, g.name, g.parent, null, null, null, null, null FROM item_groups g WHERE g.id = {rootId}
        |UNION ALL
        |SELECT cg.id, cg.name, cg.parent, null, null, null, null, null FROM item_groups cg
        |INNER JOIN group_tree ON group_tree.group_id = cg.parent)
        |SELECT * FROM group_tree) t
      |INNER JOIN item_group_items gi ON t.group_id = gi.groupId
      |INNER JOIN items i ON i.id = gi.itemId
    """.stripMargin)
  //TODO: fix error with empty parent groups - they didn't appear in query result
  def getItemTree(rootGroupId: Int)(implicit conn: Connection): ItemGroup = {
    val groups = itemTreeSql.on("rootId" -> rootGroupId)
      .as(int("group_id") ~ str("group_name") ~ get[Option[Int]]("parent_id") ~ int("item_id") ~ str("item_name")
      ~ str("uom") ~ get[BigDecimal]("weight") ~ get[BigDecimal]("volume") *).foldLeft(Map[Int,ItemGroup]()){ (groupMap, row) =>
      val gid ~ gname ~ pid ~ id ~ name ~ uom ~ w ~ v = row
      val item = Item(id, name, uom, w, v)
      groupMap.get(gid) match {
        case Some(group) => groupMap + (gid -> group.copy(items = item :: group.items))
        case None => {
          val newGroup = ItemGroup(gid, gname, Nil, List(item))
          pid match {
            case Some(parentId) => {
              val parentGroup = groupMap(parentId)
              groupMap + (gid -> newGroup) + (parentId -> parentGroup.copy(childGroups = newGroup :: parentGroup.childGroups))
            }
            case None => groupMap + (gid -> newGroup)
          }
        }
      }
    }
    groups(rootGroupId)
  }
  */
}
