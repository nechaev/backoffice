package model

import model.DocumentStatus.DocumentStatus
import tools.DbProfile._
import dbProfile.simple._

import java.time.LocalDate

/**
 * Created by sgl on 01.09.14.
 */
object IncomeReports {
  case class IncomeReport(orgUnitId: Int, date: LocalDate, posNumber: Int, totalIncome: BigDecimal, totalCount: Int)

  class IncomeReportTable(tag: Tag) extends Table[IncomeReport](tag, "income_reports") {
    def orgUnitId = column[Int]("orgUnitId")
    def date = column[LocalDate]("reportDate")
    def posNumber = column[Int]("posNumber")
    def totalIncome = column[BigDecimal]("totalIncome", types.Money)
    def totalCount = column[Int]("totalCount")
    def * = (orgUnitId, date, posNumber, totalIncome, totalCount) <> (IncomeReport.tupled, IncomeReport.unapply)
    def pk = primaryKey("income_reports_pk", (orgUnitId, date, posNumber))
    def orgUnit = foreignKey("income_reports_ou_fk", orgUnitId, OrgUnits.orgUnits)(_.id)
  }

  val incomeReports = TableQuery[IncomeReportTable]

}
