package model

import java.time.Instant
import tools.DbProfile._
import dbProfile.simple._
import scala.slick.model.ForeignKeyAction.Cascade
import model.DocumentStatus._

/**
 * Created by vnechaev on 13.05.2014.
 */
object GoodsWriteoffs {
  case class GoodsWriteoff(idOpt: Option[Int], orgUnitId: Int, created: Instant, status: DocumentStatus, postedNumber: Option[String], comment: String)
  class GoodsWriteoffsTable(tag: Tag) extends Table[GoodsWriteoff](tag, "goods_writeoffs") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def orgUnitId = column[Int]("ou")
    def created = column[Instant]("created")
    def status = column[DocumentStatus]("status", types.DocumentStatus)
    def postedNumber = column[Option[String]]("postedNumber", types.DocumentNumber)
    def comment = column[String]("comment", types.Comment)
    def * = (id.?, orgUnitId, created, status, postedNumber, comment) <> (GoodsWriteoff.tupled, GoodsWriteoff.unapply)
    def orgUnit = foreignKey("goods_writeoffs_ou_pk", orgUnitId, OrgUnits.orgUnits)(_.id)
  }
  val goodsWriteoffs = TableQuery[GoodsWriteoffsTable]
  def apply(id: Int) = goodsWriteoffs.filter(_.id === id)

  case class GoodsWriteoffItem(writeoffId: Int, itemId: Int, quantity: BigDecimal)
  class GoodsWriteoffItemsTable(tag: Tag) extends Table[GoodsWriteoffItem](tag, "goods_writeoff_items") {
    def writeoffId = column[Int]("writeoff")
    def itemId = column[Int]("item")
    def quantity = column[BigDecimal]("quantity", types.Quantity)
    def * = (writeoffId, itemId, quantity) <> (GoodsWriteoffItem.tupled, GoodsWriteoffItem.unapply)
    def writeoff = foreignKey("goods_writeoff_items_parent_fk", writeoffId, goodsWriteoffs)(_.id, Cascade, Cascade)
    def item = foreignKey("goods_writeoff_items_item_fk", itemId, Items.items)(_.id)
  }
  val goodsWriteoffItems = TableQuery[GoodsWriteoffItemsTable]

}
