package model

import tools.DbProfile._
import dbProfile.simple._
import scala.slick.model.ForeignKeyAction.Cascade

/**
 * Created by vnechaev on 10.05.2014.
 */
object Suppliers {
  class SupplierItems(tag: Tag) extends Table[(Int, Int, BigDecimal)](tag, "supplier_items") {
    def orgUnitId = column[Int]("ou")
    def itemId = column[Int]("item")
    def price = column[BigDecimal]("price", types.Money)
    def * = (orgUnitId, itemId, price)
    def orgUnit = foreignKey("supplier_items_ou_fk", orgUnitId, OrgUnits.orgUnits)(_.id, Cascade, Cascade)
    def item = foreignKey("supplier_items_item_fk", itemId, Items.items)(_.id)
  }
  val supplierItems = TableQuery[SupplierItems]
}
