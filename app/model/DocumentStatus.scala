package model

/**
 * Created by vnechaev on 23.06.2014.
 */
object DocumentStatus extends Enumeration {
  type DocumentStatus = Value
  val InPreparation = Value("I")
  val Draft = Value("D")
  val ReadyToPost = Value("R")
  val Posted = Value("P")
}
