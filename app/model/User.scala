package model

import scala.slick.model.ForeignKeyAction.Cascade
import tools.DbProfile._
import dbProfile.simple._

/**
 * Created by sgl on 28.03.14.
 */
case class User(idOpt: Option[Int], login: String, name: String) extends IdNameEntity

object Users {
  class UsersTable(tag: Tag) extends Table[User](tag, "users") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def name = column[String]("name", O.Length(50, varying = true))
    def login = column[String]("login", O.Length(30, varying = true))
    def * = (id.?, login, name) <> (User.tupled, User.unapply)
    def loginIdx = index("users_login_idx", login, unique = true)
  }
  val users = TableQuery[UsersTable]
  def apply(id: Int) = users.filter(_.id === id)

  class UserRoles(tag: Tag) extends Table[(Int, String)](tag, "roles") {
    def userId = column[Int]("userId")
    def roleId = column[String]("roleId", O.Length(20, varying = true))
    def * = (userId, roleId)
    def user = foreignKey("roles_user_fk", userId, users)(_.id, Cascade, Cascade)
  }
  val roles = TableQuery[UserRoles]
  private def userRolesQuery(userId: Int) = roles.filter(_.userId === userId).map(_.roleId)
  def userRoles(userId: Int) = userRolesQuery(userId).mapResult(Role.apply)

  class PasswordsTable(tag: Tag) extends Table[(Int, String)](tag, "passwords") {
    def userId = column[Int]("userId")
    def password = column[String]("password", O.Length(30, varying = true))
    def * = (userId, password)
    def user = foreignKey("passwords_user_fk", userId, users)(_.id)
    def userIdx = index("passwords_user_idx", userId, unique = true)
  }
  val passwords = TableQuery[PasswordsTable]

}
