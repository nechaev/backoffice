package model
import java.time.{LocalDate, Instant}
import scala.slick.model.ForeignKeyAction.Cascade
import tools.DbProfile._
import dbProfile.simple._
import model.DocumentStatus.DocumentStatus

/**
 * Created by sgl on 30.04.14.
 */
object CustomerOrders {

  case class OrderHeader(
    idOpt: Option[Int],
    author: Int,
    customer:Int,
    supplier: Int,
    created: Instant,
    shipmentDate: LocalDate,
    paymentDate: LocalDate,
    postedNumber: Option[String],
    status: DocumentStatus,
    comment: String,
    sum: BigDecimal
  )
  case class OrderItem(orderId: Int, itemId: Int, quantity: BigDecimal)

  class OrdersTable(tag: Tag) extends Table[OrderHeader](tag, "orders") {
    def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
    def authorId = column[Int]("author")
    def customerId = column[Int]("customer")
    def supplierId = column[Int]("supplier")
    def created = column[Instant]("created")
    def shipmentDate = column[LocalDate]("shipmentDate")
    def paymentDate = column[LocalDate]("paymentDate")
    def postedNumber = column[Option[String]]("postedNumber", O.Length(20, varying = true))
    def status = column[DocumentStatus]("status", types.DocumentStatus)
    def comment = column[String]("comment", types.Comment)
    def sum = column[BigDecimal]("sum", types.Money)
    def * = (id.?, authorId, customerId, supplierId, created, shipmentDate, paymentDate, postedNumber,
      status, comment, sum) <> (OrderHeader.tupled, OrderHeader.unapply)
    def author = foreignKey("orders_author_fk", authorId, Users.users)(_.id)
    def customer = foreignKey("orders_customer_fk", customerId, OrgUnits.orgUnits)(_.id)
    def supplier = foreignKey("orders_supplier_fk", supplierId, OrgUnits.orgUnits)(_.id)
  }
  val orders = TableQuery[OrdersTable]
  class OrderItemsTable(tag: Tag) extends Table[OrderItem](tag, "order_items") {
    def orderId = column[Int]("orderId")
    def itemId = column[Int]("itemId")
    def quantity = column[BigDecimal]("quantity")
    def * = (orderId, itemId, quantity) <> (OrderItem.tupled, OrderItem.unapply)
    def order = foreignKey("order_items_order_fk", orderId, orders)(_.id, Cascade, Cascade)
    def item = foreignKey("order_items_item_fk", itemId, Items.items)(_.id)
    def orderItemPk = primaryKey("order_items_pk", (orderId, itemId))
  }
  val orderItems = TableQuery[OrderItemsTable]

}
