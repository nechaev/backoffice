package model

import tools.DbProfile._
import dbProfile.simple._

/**
 * Created by sgl on 02.05.14.
 */

object Customers {
  case class Customer(customerId: Int, supplierId: Int, catalogGroupId: Int)
  class CustomersTable(tag: Tag) extends Table[Customer](tag, "customers") {
    def customerId = column[Int]("customer")
    def supplierId = column[Int]("supplier")
    def catalogGroupId = column[Int]("catalogItemGroup")
    def * = (customerId, supplierId, catalogGroupId) <> (Customer.tupled, Customer.unapply)
    def customer = foreignKey("customers_customer_fk", customerId, OrgUnits.orgUnits)(_.id)
    def supplier = foreignKey("customers_supplier_fk", supplierId, OrgUnits.orgUnits)(_.id)
    def catalogGroup = foreignKey("customers_catalog_group_fk", catalogGroupId, Items.itemGroups)(_.id)
  }
  val customers = TableQuery[CustomersTable]
}
