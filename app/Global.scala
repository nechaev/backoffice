import play.api.{Logger, Play, Application, GlobalSettings}
import play.api.Play.current
import tools.DatabaseBuilder
import tools.DbProfile._
import dbProfile.simple._

/**
 * Created by sgl on 05.05.14.
 */
object Global extends GlobalSettings {
  override def onStart(app: Application) {
    if (Play.configuration.getBoolean("application.createdb").filter(v => v) != None) {
      database.withSession { implicit sess =>
        Logger.info("Creating database schema")
        DatabaseBuilder.initDb
      }
    }
  }
}
