package views

import model.DocumentStatus

/**
 * Created by vnechaev on 24.06.2014.
 */
object Tools {
  def orderStatusIcon(status: DocumentStatus.DocumentStatus) = status match {
    case DocumentStatus.Draft => "edit"
    case DocumentStatus.ReadyToPost => "time"
    case DocumentStatus.Posted => "ok"
  }
}
