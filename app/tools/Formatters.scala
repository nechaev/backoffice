package tools

import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalAccessor
import java.time.{ZoneId, Instant}
import java.text.{NumberFormat, DecimalFormat}

/**
 * Created by sgl on 31.03.14.
 */
object Formatters {

  private def temporalValue(v: TemporalAccessor): TemporalAccessor = v match {
    case i: Instant => i.atZone(ZoneId.systemDefault())
    case zv => zv
  }

  val dateFormat = DateTimeFormatter.ofPattern("dd.MM.yyyy")
  val timeFormat = DateTimeFormatter.ofPattern("HH:mm:ss")
  val dateTimeFormat = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss")

  def date(v: TemporalAccessor) = dateFormat.format(temporalValue(v))
  def time(v: TemporalAccessor) = timeFormat.format(temporalValue(v))
  def dateTime(v: TemporalAccessor) = dateTimeFormat.format(temporalValue(v))

  val currencyFormat = new DecimalFormat("#0.00")
  val quantityFormat = new DecimalFormat("#0.000")

  def currency(v: BigDecimal, zeroEmpty: Boolean = false) = if (zeroEmpty && v == BigDecimal(0)) "" else currencyFormat.format(v.bigDecimal)
  def quantity(v: BigDecimal, zeroEmpty: Boolean = false) = if (zeroEmpty && v == BigDecimal(0)) "" else quantityFormat.format(v.bigDecimal)

}
