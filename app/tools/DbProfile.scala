package tools

import play.api.Play.current
import scala.slick.ast.ColumnOption
import scala.slick.driver.PostgresDriver
import java.time.{LocalDate, Instant}
import java.sql.{Date, Timestamp}
import model.DocumentStatus
import model.DocumentStatus.DocumentStatus

/**
 * Created by sgl on 01.05.14.
 */

object DbProfile {
  val dbProfile = PostgresDriver.profile
  import dbProfile.simple._

  implicit val instantColumnType = MappedColumnType.base[Instant, Timestamp] (
    { i => Timestamp.from(i) },
    { t => t.toInstant }
  )
  implicit val localDateColumnType = MappedColumnType.base[LocalDate, Date] (
    { d => Date.valueOf(d) },
    { d => d.toLocalDate }
  )
  implicit val docStatusColumnType = MappedColumnType.base[DocumentStatus, String] (
    { s => s.toString },
    { s => DocumentStatus.withName(s) }
  )
  def database = Database.forDataSource(play.api.db.DB.getDataSource())

  def numeric(int: Int) = s"NUMERIC($int)"
  def numeric(int: Int, frac: Int) = s"NUMERIC($int,$frac)"

  object types {
    val Money = ColumnOption.DBType(numeric(13, 2))
    val Quantity = ColumnOption.DBType(numeric(13, 3))
    val Volume = ColumnOption.DBType(numeric(10, 3))
    val Weight = ColumnOption.DBType(numeric(10, 3))
    val DocumentStatus = ColumnOption.Length(1, varying = false)
    val Comment = ColumnOption.Length(150, varying = true)
    val DocumentNumber = ColumnOption.Length(20, varying = true)
  }
}