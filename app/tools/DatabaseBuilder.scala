package tools

import model._
import DbProfile.dbProfile.simple._

/**
 * Created by sgl on 05.05.14.
 */
object DatabaseBuilder {
  val tables = Seq(
    Users.users,
    Users.passwords,
    Users.roles,
    OrgUnits.orgUnits,
    OrgUnits.userOrgUnits,
    Customers.customers,
    Items.items,
    Items.itemGroups,
    Items.itemGroupItems,
    CustomerOrders.orders,
    CustomerOrders.orderItems,
    Suppliers.supplierItems,
    GoodsReceipts.goodsReceipts,
    GoodsReceipts.goodsReceiptsItems,
    GoodsWriteoffs.goodsWriteoffs,
    GoodsWriteoffs.goodsWriteoffItems,
    RoyaltyBills.royaltyBills,
    RoyaltyBills.royaltyBillForms,
    Bills.bills,
    Bills.billForms,
    ReportingPeriods.reportingPeriods,
    IncomeReports.incomeReports
  )

  lazy val ddls = tables.map(_.ddl).reduce(_ ++ _)
  def printDDLStatements = ddls.createStatements.foreach(println)

  def initDb(implicit sess: Session) {
    printDDLStatements
    //ddls.drop
    ddls.create
    //val adminId = (Users.users returning Users.users.map(_.id)) += User(None, "admin", "Admin")
    //Users.passwords += (adminId, "1234")

  }

}
