package controllers

import model.{Role, User}

/**
 * Created by sgl on 03.04.14.
 */
sealed trait AuthState

object Unauthenticated extends AuthState

case class Authenticated(user: User, roles: Set[Role]) extends AuthState
