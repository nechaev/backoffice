package controllers

import model.Role.Admin

/**
 * Created by sgl on 15.04.14.
 */
object OrgUnitsAdmin extends AbstractController {

  def orgUnitList = authenticated(Admin) { implicit ctx =>
    Ok
  }

  def orgUnit(id: Int) = authenticated(Admin) { implicit ctx =>
    Ok
  }

  def newOrgUnit = authenticated(Admin) { implicit ctx =>
    Ok
  }

  def updateOrgUnit = authenticated(Admin) { implicit ctx =>
    Ok
  }

}
