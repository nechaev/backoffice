package controllers

import play.api.mvc._
import model.{Role, Users}
import tools.DbProfile._
import dbProfile.simple._

/**
 * Created by sgl on 03.04.14.
 */
abstract class AbstractController extends Controller {

  def action(block: AuthRequestContext => Result) = Action { req =>
    val authState = database.withSession { implicit sess =>
      req.session.get("userId").flatMap(userId => Users(userId.toInt).firstOption) match {
        case Some(user) => Authenticated(user, Set(Users.userRoles(user.id).list:_*))
        case None => Unauthenticated
      }
    }
    block(new AuthRequestContext(req, authState))
  }

  def authenticated(block: AuthRequestContext => Result) = action { ctx =>
    ctx.authState match {
      case Authenticated(_, _) => block(ctx)
      case Unauthenticated => Redirect(routes.Authentication.login()).withNewSession
    }
  }

  def authenticated(role: Role)(block: AuthRequestContext => Result) = action { ctx =>
    ctx.authState match {
      case Authenticated(_, roles) => if (roles.contains(role)) block(ctx) else Forbidden
      case Unauthenticated => Redirect(routes.Authentication.login()).withNewSession
    }
  }

}
