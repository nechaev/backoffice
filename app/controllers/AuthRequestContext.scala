package controllers

import play.api.mvc.{AnyContent, Result, Request}
import controllers.NotificationLevel.NotificationLevel

/**
 * Created by sgl on 03.04.14.
 */
class AuthRequestContext(val request: Request[AnyContent], val authState: AuthState) {
  implicit def req = request
  def session = req.session
  def user = authState match {
    case Authenticated(user, _) => user
    case _ => throw new IllegalStateException("Not authenticated")
  }
  def isAuthenticated = authState != Unauthenticated
  lazy val notification = request.flash.get("notification-message").map(msg =>
    Notification(msg, request.flash.get("notification-level").map(NotificationLevel.withName)
      .getOrElse(NotificationLevel.Info))
  )
}

case class Notification(message: String, level: NotificationLevel)

object NotificationLevel extends Enumeration {
  type NotificationLevel = Value
  val Success = Value("success")
  val Info = Value("info")
  val Warning = Value("warning")
  val Error = Value("danger")
}

class NotificationSupport(result: Result) {
  def withNotification(message:String, level: NotificationLevel = NotificationLevel.Info) = {
    result.flashing("notification-message" -> message, "notification-level" -> level.toString)
  }
}