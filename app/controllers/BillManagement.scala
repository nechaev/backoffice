package controllers

import java.time.LocalDate
import model.Role.Customer
import tools.DbProfile._
import dbProfile.simple._
import model.Bills.{bills, billForms}
import play.api.mvc.{ResponseHeader, Result}
import play.api.libs.iteratee.Enumerator
import model.CustomerOrders
import scala.language.postfixOps

/**
 * Created by vnechaev on 26.06.2014.
 */
object BillManagement extends AbstractController {
  case class BillListItem(id: Int, date: LocalDate, number: String, orgUnit: String, supplier: String, orderId: Option[Int],
                          orderNumber: Option[String], sum: BigDecimal, paid: BigDecimal, comment: String)
  def list = authenticated(Customer) { implicit ctx => database.withSession { implicit sess =>
    val billsAndOrders = bills.filter(b => orgUnitAllowed(b.orgUnitId)).sortBy(_.date desc) leftJoin CustomerOrders.orders on (_.orderId === _.id)
    val billItems = (for {
      (bill, order) <- billsAndOrders
      orgUnitName <- bill.orgUnit.map(_.name)
      supplierName <- bill.supplier.map(_.name)
    } yield (bill.id, bill.date, bill.number, orgUnitName, supplierName, bill.orderId.?, order.postedNumber,
        bill.sum, bill.paid, bill.comment))
      .sortBy(_._2.desc).take(maxListResultLimit).mapResult(BillListItem.tupled).list
    Ok(views.html.billList(billItems))
  } }

  def form(billId: Int) = authenticated(Customer) { implicit ctx => database.withSession { implicit sess =>
    billForms.filter(form => form.billId === billId &&
      form.bill.filter(b => orgUnitAllowed(b.orgUnitId)).exists
    ).map(_.formData).firstOption match {
      case Some(bytes) => Result(
        header = ResponseHeader(200, Map(CONTENT_TYPE -> "application/pdf")),
        body = Enumerator(bytes)
      )
      case None => Redirect(routes.BillManagement.list()).withNotification("Выбранная форма отсутствует", NotificationLevel.Error)
    }
  } }
}
