import java.time.LocalDate
import java.time.format.DateTimeParseException
import java.time.temporal.TemporalQueries
import java.util.NoSuchElementException

import model.OrgUnits
import play.api.data.FormError
import play.api.data.format.Formatter
import play.api.libs.json._
import play.api.mvc.Result
import tools.DbProfile._
import dbProfile.simple._
import tools.Formatters
import scala.language.implicitConversions

/**
 * Created by sgl on 16.04.14.
 */
package object controllers {
  implicit def withNotificationSupport(result: Result) = new NotificationSupport(result)

  def strOpt(s: String) = if (s.isEmpty) None else Some(s)
  def dateOpt(s: String) = if (s.isEmpty) None else Some(Formatters.dateFormat.parse(s, TemporalQueries.localDate()))

  implicit def localDateFormat = new Formatter[LocalDate] {
    override def bind(key: String, data: Map[String, String]): Either[Seq[FormError], LocalDate] = try {
      Right(Formatters.dateFormat.parse(data(key), TemporalQueries.localDate()))
    } catch {
      case ex: DateTimeParseException => Left(Seq(FormError(key, "Date parse error")))
      case ex: NoSuchElementException => Left(Seq(FormError(key, "Field not found")))
      case _: Throwable => Left(Seq(FormError(key, "Bind error")))
    }
    override def unbind(key: String, value: LocalDate): Map[String, String] = Map(key -> Formatters.dateFormat.format(value))
  }

  object localDateReads extends Reads[LocalDate] {
    override def reads(json: JsValue): JsResult[LocalDate] = json match {
      case JsString(v) => try {
        JsSuccess(Formatters.dateFormat.parse(v, TemporalQueries.localDate()))
      } catch {
        case e: Throwable => JsError(e.getMessage)
      }
      case _ => JsError("Expected String value")
    }
  }
  object localDateWrites extends Writes[LocalDate] {
    override def writes(o: LocalDate): JsValue = JsString(Formatters.dateFormat.format(o))
  }
  implicit val localDateFormats = Format(localDateReads, localDateWrites)

  def orgUnitAllowed(ouId: Column[Int])(implicit ctx: AuthRequestContext) = {
    OrgUnits.userOrgUnits.filter(uou => uou.userId === ctx.user.id && uou.orgUnitId === ouId).exists
  }

  val maxListResultLimit = 500
  val maxItemSelectLimit = 25

}
