package controllers

import java.time.{LocalDate, Instant}
import model._
import play.api.data.Form
import play.api.data.Forms._
import play.api.data.format.Formats._
import model.CustomerOrders.{OrderItem, OrderHeader}
import model.Role.Customer
import play.api.libs.json.JsNumber
import tools.DbProfile._
import dbProfile.simple._
import model.DocumentStatus.DocumentStatus

/**
 * Created by sgl on 30.03.14.
 */
object CustomerOrdersManagement extends AbstractController {

  case class OrderListItem
  (
    id: Int,
    created: Instant,
    customer: String,
    supplier: String,
    status: DocumentStatus,
    number: Option[String],
    shipmentDate: LocalDate,
    comment: String,
    sum: BigDecimal
  )
  case class OrderItemViewModel(
    orderItem: OrderItem,
    item: Item
  ) {
    def sum = orderItem.quantity * item.price
    def volume = orderItem.quantity * item.volume
    def weight = orderItem.quantity * item.weight
  }
  case class OrderViewModel(
    header: OrderHeader,
    customerName: String,
    supplierName: String,
    items: Seq[OrderItemViewModel]
  ) {
    private def agg(field: OrderItemViewModel => BigDecimal) = items
      .foldLeft(BigDecimal(0))((acc, item) => acc + field(item))
    def sum = agg(_.sum)
    def volume = agg(_.volume)
    def weight = agg(_.weight)
  }

  def allowedOrderById(orderId: Int)(implicit ctx: AuthRequestContext) = {
    CustomerOrders.orders.filter(o => o.id === orderId && orgUnitAllowed(o.customerId))
  }
  def editableOrderById(orderId: Int)(implicit ctx: AuthRequestContext) = {
    allowedOrderById(orderId).filter(o => o.status === DocumentStatus.InPreparation || o.status === DocumentStatus.Draft)
  }

  def orders = authenticated(Customer) { implicit ctx => database.withSession { implicit sess =>
    val orders = (for {
      order <- CustomerOrders.orders if orgUnitAllowed(order.customerId) && order.status =!= DocumentStatus.InPreparation
      supplier <- order.supplier.map(_.name)
      customer <- order.customer.map(_.name)
    } yield {
      (order.id, order.created, customer, supplier, order.status, order.postedNumber, order.shipmentDate, order.comment, order.sum)
    }).sortBy(_._2.desc).take(maxListResultLimit).mapResult(OrderListItem.tupled).list
    Ok(views.html.orderList(orders))
  } }

  private def userCustomersQuery(userId: Int)(implicit ctx: AuthRequestContext) = for {
    customer <- Customers.customers if orgUnitAllowed(customer.customerId)
    customerOU <- customer.customer
    supplierOU <- customer.supplier
  } yield (customerOU, supplierOU)

  def prepareNew = authenticated(Customer) { implicit ctx => database.withTransaction { implicit sess =>
    CustomerOrders.orders.filter(order => order.authorId === ctx.user.id &&
      order.status === DocumentStatus.InPreparation).delete
    Customers.customers.filter(c => orgUnitAllowed(c.customerId)).map(c => (c.customerId, c.supplierId)).firstOption.map {
      case (customerId, supplierId) => {
        val newOrder = OrderHeader(None, ctx.user.id, customerId, supplierId,
              Instant.now(), LocalDate.now(), LocalDate.now(), None, DocumentStatus.InPreparation, "", 0)
            (CustomerOrders.orders returning CustomerOrders.orders.map(_.id)) += newOrder
      }
    } match {
      case Some(id) => Redirect(routes.CustomerOrdersManagement.header(id))
      case None => Redirect(routes.CustomerOrdersManagement.orders()).withNotification("Создание нового заказа невозможно", NotificationLevel.Error)
    }
  } }

  def header(orderId: Int) = authenticated(Customer) { implicit ctx => database.withSession { implicit sess =>
    val customers = userCustomersQuery(ctx.user.id).list
    val customerOUs = Set(customers.map(_._1): _*).toList
    val supplierOUs = Set(customers.map(_._2): _*).toList
    if (customers.isEmpty) Redirect(routes.CustomerOrdersManagement.orders())
    else {
      val (selCustomer, selSupplier) = allowedOrderById(orderId).map(order => (order.customerId, order.supplierId)).first
      if (customers.length == 1) Redirect(routes.CustomerOrdersManagement.orderItemRootCatalog(orderId))
      else Ok(views.html.orderHeader(orderId, customerOUs, selCustomer, supplierOUs, selSupplier,
        customers.groupBy(_._1.id).mapValues(_.map(_._2.id))
      ))
    }
  } }

  private val headerForm = Form(tuple(
    "id" -> number,
    "customer" -> number,
    "supplier" -> number
  ))

  def saveHeader = authenticated(Customer) { implicit ctx =>
    import ctx.req
    val (orderId, customerId, supplierId) = headerForm.bindFromRequest().get
    database.withSession { implicit sess => rootGroupId(customerId, supplierId) match {
      case Some(rootGroupId) => {
        val updated = CustomerOrders.orders.filter(o => o.id === orderId && (o.customerId =!= customerId || o.supplierId =!= supplierId))
          .map(o => (o.customerId, o.supplierId, o.sum)).update((customerId, supplierId, BigDecimal(0)))
        if (updated > 0) CustomerOrders.orderItems.filter(_.orderId === orderId).delete
        Redirect(routes.CustomerOrdersManagement.orderItemCatalog(orderId, rootGroupId))
      }
      case None => Redirect(routes.CustomerOrdersManagement.header(orderId))
        .withNotification("Для выбранных поставщика и покупателя оформление заказа невозможно",
          NotificationLevel.Error)
    }
  } }

  private def rootGroupId(cid: Int, sid: Int)(implicit sess: Session) = Customers.customers
    .filter(c => c.customerId === cid && c.supplierId === sid)
    .map(_.catalogGroupId)
    .firstOption

  def orderItemRootCatalog(orderId: Int) = authenticated(Customer) { implicit ctx => database.withSession { implicit sess =>
    val order = allowedOrderById(orderId).first
    Redirect(routes.CustomerOrdersManagement.orderItemCatalog(orderId,
        rootGroupId(order.customer, order.supplier).get
    ))
  } }

  def orderItemCatalog(orderId: Int, groupId: Int) = authenticated { implicit ctx => database.withSession { implicit sess =>
    val (customer, supplier, orderSum) = (for {
      order <- allowedOrderById(orderId)
      customer <- order.customer
      supplier <- order.supplier
    } yield (customer, supplier, order.sum)).first

    val items = (Items.groupItems(groupId).sortBy(_.name) leftJoin CustomerOrders.orderItems on ((i, o) => i.id === o.itemId && o.orderId === orderId))
    .map({
      case (i, o) => (i, o.quantity.?)
    }).sortBy(_._1.name).mapResult({
      case (item, quantityOpt) => OrderItemViewModel(CustomerOrders.OrderItem(orderId, item.id, quantityOpt.getOrElse(BigDecimal(0))), item)
    }).list

    Ok(views.html.orderItemCatalog(Items.getGroupPath(groupId, rootGroupId(customer.id, supplier.id).get),
          Items.childGroups(groupId).sortBy(_.name).list, items, orderId, customer.name, supplier.name, orderSum))

  } }

  private val itemAmountForm = Form(tuple(
    "id" -> number,
    "item" -> number,
    "qty" -> of[BigDecimal]
  ))

  def updateItemAmount() = authenticated(Customer) { implicit ctx => database.withTransaction { implicit sess =>
    import ctx.req
    val (orderId, itemId, quantity) = itemAmountForm.bindFromRequest().get
    val order = editableOrderById(orderId).first
    CustomerOrders.orderItems.filter(i => i.orderId === orderId && i.itemId === itemId).delete
    if (quantity != BigDecimal(0)) CustomerOrders.orderItems += OrderItem(order.idOpt.get, itemId, quantity)
    val orderSum = Query((for {
      oi <- CustomerOrders.orderItems if oi.orderId === orderId
      item <- oi.item
    } yield oi.quantity * item.price).sum).first.getOrElse(BigDecimal(0))
    CustomerOrders.orders.filter(_.id === orderId).map(_.sum).update(orderSum)
    Ok(JsNumber(orderSum))
  } }

  def properties(orderId: Int) = authenticated(Customer) { implicit ctx => database.withSession { implicit sess =>
    val orderModel = (for {
      header <- allowedOrderById(orderId)
      customer <- header.customer.map(_.name)
      supplier <- header.supplier.map(_.name)
    } yield (header, customer, supplier)).mapResult({
      case (header, customer, supplier) => OrderViewModel(header, customer, supplier, (for {
        orderItem <- CustomerOrders.orderItems.filter(_.orderId === orderId)
        item <- orderItem.item
      } yield (orderItem, item)).mapResult(OrderItemViewModel.tupled).list
      )
    }).first
    Ok(views.html.orderView(orderModel))
  } }

  private val submitOrderForm = Form(tuple(
    "id" -> number,
    "shipmentDate" -> of[LocalDate],
    "comment" -> text,
    "commit" -> text
  ))
  def saveProperties = authenticated(Customer) { implicit ctx => database.withTransaction { implicit sess =>
    import ctx.req
    val (orderId, shipmentDate, comment, commit) = submitOrderForm.bindFromRequest().get
    editableOrderById(orderId).map(order => (order.shipmentDate, order.comment, order.status, order.created))
      .update((shipmentDate, comment, DocumentStatus.Draft, Instant.now()))
    if (commit equals "true") editableOrderById(orderId).map(_.status).update(DocumentStatus.ReadyToPost)
    Redirect(routes.CustomerOrdersManagement.orders())
      .withNotification("Заказ успешно сохранен", NotificationLevel.Success)
  } }

  def delete(orderId: Int) = authenticated(Customer) { implicit ctx => database.withTransaction { implicit sess =>
    val deleted = editableOrderById(orderId).delete
    val redirect = Redirect(routes.CustomerOrdersManagement.orders())
    if (deleted == 0) redirect.withNotification("Удаление заказа невозможно", NotificationLevel.Error)
    else redirect.withNotification("Заказ успешно удален", NotificationLevel.Success)
  } }

}
