package controllers

import java.time.Instant
import model.DocumentStatus.DocumentStatus
import model.DocumentStatus.DocumentStatus
import model.Role.Warehouse
import tools.DbProfile._
import dbProfile.simple._
import model.OrgUnits._
import model.GoodsWriteoffs._
import model.{DocumentStatus, Items, Item}
import play.api.libs.json.Json
import play.api.data.Form
import play.api.data.Forms._
import model.GoodsWriteoffs.GoodsWriteoff
import model.GoodsWriteoffs.GoodsWriteoffItem

/**
 * Created by vnechaev on 13.05.2014.
 */
object GoodsWriteoffManagement extends AbstractController {
  case class WriteoffListItem(id: Int, created: Instant, orgUnit: String, status: DocumentStatus, postedNumber: Option[String])

  def list = authenticated(Warehouse) { implicit ctx => database.withSession { implicit sess =>
    val listItems = (for {
      wo <- goodsWriteoffs.sortBy(_.created.desc) if userOrgUnitsForUser(ctx.user.id).filter(_.orgUnitId === wo.orgUnitId).exists
      orgUnitName <- wo.orgUnit.map(_.name)
    } yield (wo.id, wo.created, orgUnitName, wo.status, wo.postedNumber)).take(maxListResultLimit).mapResult(WriteoffListItem.tupled).list
    Ok(views.html.goodsWriteoffList(listItems))
  } }

  def allowedWriteoffById(writeoffId: Int)(implicit ctx: AuthRequestContext) = {
    goodsWriteoffs.filter(wo => wo.id === writeoffId && orgUnitAllowed(wo.orgUnitId))
  }
  def editableWriteoffById(writeoffId: Int)(implicit ctx: AuthRequestContext) = {
    allowedWriteoffById(writeoffId).filter(_.status === DocumentStatus.Draft)
  }

  private def writeoffEditForm(writeoff: GoodsWriteoff, items: Seq[(GoodsWriteoffItem, Item)])(implicit sess: Session, ctx: AuthRequestContext) = {
    val orgUnits = orgUnitsForUser(ctx.user.id).list
    Ok(views.html.goodsWriteoff(writeoff, items, orgUnits))
  }

  def editWriteoff(id: Int) = authenticated(Warehouse) { implicit ctx => database.withSession { implicit sess =>
    val writeoff = allowedWriteoffById(id).first
    val writeoffItems = (for {
      writeoffItem <- goodsWriteoffItems.filter(_.writeoffId === id)
      item <- writeoffItem.item
    } yield (writeoffItem, item)).list
    writeoffEditForm(writeoff, writeoffItems)
  } }

  def newWriteoff = authenticated(Warehouse) { implicit ctx => database.withSession { implicit sess =>
    val defaultOrgUnitId = userOrgUnitsForUser(ctx.user.id).map(_.orgUnitId).first
    writeoffEditForm(GoodsWriteoff(None, defaultOrgUnitId, Instant.now(), DocumentStatus.Draft, None, ""), Nil)
  } }

  val writeoffForm = Form(tuple(
    "id" -> optional(number),
    "orgUnit" -> number,
    "comment" -> text,
    "writeoffItems" -> text,
    "commit" -> text
  ) )

  case class WriteoffItemLine(item: Int, quantity: BigDecimal)
  implicit val writeoffItemReads = Json.reads[WriteoffItemLine]
  def saveWriteoff = authenticated(Warehouse) { implicit ctx => database.withTransaction { implicit sess =>
    import ctx.req
    writeoffForm.bindFromRequest().fold(
      withErrors => BadRequest(withErrors.errorsAsJson), {
      case (idOpt, orgUnit, comment, itemsJson, commit) =>
        val items = Json.parse(itemsJson).as[List[WriteoffItemLine]]
        val redirect = Redirect(routes.GoodsWriteoffManagement.list)
        val writeoffId = idOpt match {
          case Some(updId) =>
            val updated = editableWriteoffById(updId).map(w => (w.orgUnitId, w.comment)).update((orgUnit, comment))
            if (updated > 0) {
              goodsWriteoffItems.filter(_.writeoffId === updId).delete
              Some(updId)
            } else None
          case None =>
            val newId = (goodsWriteoffs returning goodsWriteoffs.map(_.id)) += GoodsWriteoff(None, orgUnit, Instant.now(),
              DocumentStatus.Draft, None, comment)
            Some(newId)
        }
        writeoffId match {
          case Some(id) =>
            goodsWriteoffItems ++= items.map(item => GoodsWriteoffItem(id, item.item, item.quantity))
            if (commit equals "true") goodsWriteoffs.filter(_.id === id).map(_.status).update(DocumentStatus.ReadyToPost)
            redirect.withNotification("Документ списания сохранен", NotificationLevel.Success)
          case None => redirect.withNotification("Документ не может быть изменен", NotificationLevel.Error)
        }
      }
    )
  } }

  def itemList(filter: String) = authenticated(Warehouse) { implicit ctx => database.withSession { implicit sess =>
    implicit val itemWrites = Json.writes[Item]
    val items = Items.items.filter(_.name.toLowerCase.startsWith(filter.toLowerCase)).sortBy(_.name).take(maxItemSelectLimit).list
    Ok(Json.toJson(items))
  } }

  def itemLine(itemId: Int) = authenticated(Warehouse) { implicit ctx => database.withSession { implicit sess =>
    val item = Items(itemId).first
    Ok(views.html.goodsWriteoffItems(Seq((GoodsWriteoffItem(0, itemId, 0), item))))
  } }
  def delete(id: Int) = authenticated(Warehouse) { implicit ctx => database.withTransaction { implicit sess =>
    val deleted = editableWriteoffById(id).delete
    val redirect = Redirect(routes.GoodsWriteoffManagement.list)
    if (deleted > 0) redirect.withNotification("Документ удален", NotificationLevel.Success)
    else redirect.withNotification("Документ не может быть удален", NotificationLevel.Error)
  } }

}
