package controllers

import java.time.{DayOfWeek, LocalDate}
import java.time.format.{DateTimeParseException, DateTimeFormatter}
import java.util.NoSuchElementException
import java.util.concurrent.TimeUnit
import model.DocumentStatus
import model.Role.Customer
import play.api.data.{FormError, Form}
import play.api.data.Forms._
import play.api.data.format.Formats._
import play.api.data.format.Formatter
import tools.DbProfile._
import dbProfile.simple._
import tools.Formatters
import scala.slick.jdbc.StaticQuery.interpolation

/**
 * Created by sgl on 01.09.14.
 */
object IncomeReportController extends AbstractController {
  case class IncomeReportItem(posNumber: Int, income: BigDecimal, count: Int, editable: Boolean)
  case class IncomeReportRow(date: LocalDate, items: Seq[IncomeReportItem])

  import model.ReportingPeriods._
  import model.IncomeReports._
  import model.OrgUnits._

  def defaultIncomeReport = authenticated(Customer) { implicit ctx => database.withSession { implicit sess =>
    val period = reportingPeriods.filter(_.start < LocalDate.now()).sortBy(_.start.desc).take(1).first
    val orgUnitId = userOrgUnits.filter(_.userId === ctx.user.id).map(_.orgUnitId).take(1).first
    Redirect(routes.IncomeReportController.incomeReportView(orgUnitId, period.year, period.number))
  } }

  def isEditable(d: LocalDate) = {
    val now = LocalDate.now()
    val weekStart = LocalDate.from(DayOfWeek.MONDAY.adjustInto(now))
    if (now.isAfter(weekStart)) !d.isBefore(weekStart)
    else !d.isBefore(weekStart.minusWeeks(1))
  }

  def incomeReportView(orgUnitId: Int, year: Int, periodNr: Int) = authenticated(Customer) { implicit ctx => database.withSession { implicit sess =>
    val period = reportingPeriods.filter(p => p.year === year && p.number === periodNr).first
    val posCount = orgUnits.filter(_.id === orgUnitId).map(_.posCount).first
    val reportRows = incomeReports.filter(r => r.orgUnitId === orgUnitId && r.date.between(period.start, period.end)).list
    val rowsByDate = reportRows.groupBy(_.date)
    var dates = List(period.start)
    while (dates.head isBefore period.end) dates = dates.head.plusDays(1) :: dates
    val displayRows = dates.reverse.map(date => rowsByDate.get(date) match {
      case Some(rows) =>
        val rowsByNumber = rows.groupBy(_.posNumber)
        val items = (1 to posCount).map(posIndex => rowsByNumber.get(posIndex) match {
          case Some(subRows) => IncomeReportItem(posIndex, subRows.head.totalIncome, subRows.head.totalCount, isEditable(date))
          case None => IncomeReportItem(posIndex, BigDecimal(0), 0, isEditable(date))
        })
        IncomeReportRow(date, items)
      case None => IncomeReportRow(date, (1 to posCount).map(posIndex => IncomeReportItem(posIndex, BigDecimal(0), 0, isEditable(date))))
    })
    val years = sql"select distinct year from reporting_periods order by year".as[Int].list
    val periods = sql"select distinct number from reporting_periods order by number".as[Int].list
    val orgUnitList = orgUnitsForUser(ctx.user.id).list
    Ok(views.html.incomeReport(displayRows, period, years, periods, orgUnitId, orgUnitList))
  } }

  private val updateForm = Form(tuple(
    "orgUnitId" -> number,
    "date" -> of[LocalDate],
    "posNumber" -> number,
    "sum" -> of[BigDecimal],
    "count" -> number
  ))

  def updateIncomeReport = authenticated(Customer) { implicit ctx => database.withTransaction { implicit sess =>
    import ctx.req
    val (ou, date, posNumber, sum, count) = updateForm.bindFromRequest().get
    if (isEditable(date)) {
      incomeReports.filter(r => r.orgUnitId === ou && r.date === date && r.posNumber === posNumber).delete
      incomeReports += IncomeReport(ou, date, posNumber, sum, count)
      Ok
    } else BadRequest
  } }

}
