package controllers

import java.io.FileInputStream
import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalQueries
import java.time.{Instant, ZoneId}

import com.typesafe.scalalogging.LazyLogging
import model.IncomeReports.IncomeReport
import model.Items.ItemGroup
import model.RoyaltyBills.RoyaltyBillForm
import model.{Item, _}
import play.api.Play
import play.api.Play.current
import play.api.libs.Files
import play.api.mvc._
import tools.DbProfile._
import tools.DbProfile.dbProfile.simple._

import scala.xml.{Node, NodeSeq}

/**
 * Created by sgl on 08.04.14.
 */
object Sync extends Controller with LazyLogging {

  private val syncKey = Play.configuration.getString("sync.key").get

  def syncAllowed(block: =>Result)(implicit req: Request[_]) = {
    if (req.headers.get("syncKey").equals(Some(syncKey))) block else Forbidden
  }

  def syncItems = Action(parse.xml) { implicit req => syncAllowed { database.withTransaction { implicit sess =>
    for (elem <- req.body \\ "item") syncItem(elem)
    Ok
  } } }

  private def asBigDecimal(node: NodeSeq) = {
    val textValue = node.text
    try {
      BigDecimal(textValue)
    } catch {
      case e: NumberFormatException =>
        if (node.isEmpty) throw new IllegalArgumentException("Node is empty")
        logger.error(s"Cannot parse $textValue as BigDecimal")
        throw e
    }
  }
  //private def asInstant(node: NodeSeq) = Instant.parse(node.text)
  private def asInt(node: NodeSeq) = node.text.toInt


  def syncItem(elem: Node)(implicit sess: dbProfile.simple.Session) = {
    val uom = (elem \ "uom").text
    val weight = asBigDecimal(elem \ "weight")
    val volume = asBigDecimal(elem \ "volume")
    val price = asBigDecimal(elem \ "price")
    val name = (elem \ "name").text
    val syncCode = (elem \ "code").text

    logger.info(s"Updating item $name")
    val itemId = Items.items.filter(_.syncCode === syncCode).map(_.id).firstOption match {
      case Some(id) =>
        Items(id).map(i => (i.syncCode, i.name, i.uom, i.weight, i.volume, i.price)).update((syncCode, name, uom, weight, volume, price))
        logger.debug(s"Updated item $id")
        id
      case None =>
        val id = (Items.items returning Items.items.map(_.id)) += Item(None, Some(syncCode), name, uom, weight, volume, price)
        logger.debug(s"Inserted item $id")
        id
    }
    for (pathElem <- elem \ "path") {
      val rootGroup = (pathElem \ "root").text.toInt
      val itemGroup = (pathElem \ "group").foldLeft(rootGroup)((groupId, node) => {
        Items.itemGroups.filter(g => g.name === node.text && g.parentId === groupId).map(_.id).firstOption match {
          case Some(childGroupId) => childGroupId
          case None =>
            val id = (Items.itemGroups returning Items.itemGroups.map(_.id)) += ItemGroup(None, node.text, Some(groupId))
            logger.debug(s"Inserted group $id")
            id
        }
      })
      if (!Query(Items.itemGroupItems.filter(row => row.itemId === itemId && row.groupId === itemGroup).exists).first) {
        Items.itemGroupItems += (itemGroup, itemId)
      }
    }
  }

  val dtf = DateTimeFormatter.ISO_LOCAL_DATE_TIME
  val df = DateTimeFormatter.ISO_LOCAL_DATE

  val pendingOrdersQuery = for {
    orderHeader <- CustomerOrders.orders.filter(_.status === DocumentStatus.ReadyToPost)
    customerName <- orderHeader.customer.map(_.name)
    supplierName <- orderHeader.supplier.map(_.name)
  } yield {
    (orderHeader, customerName, supplierName)
  }

  def getPendingOrders = Action { implicit req => syncAllowed { database.withSession { implicit sess =>
    val orders = pendingOrdersQuery.list.map({ row =>
      val (order, customer, supplier) = row
      <order id={order.idOpt.get.toString} customer={customer} supplier={supplier}
             created={dtf.format(order.created.atZone(ZoneId.systemDefault()))}
             shipmentDate={df.format(order.shipmentDate)} paymentDate={df.format(order.paymentDate)} comment={order.comment}>
        { (for {
        orderItem <- CustomerOrders.orderItems if orderItem.orderId === order.idOpt.get
        itemCode <- orderItem.item.map(_.syncCode)
      } yield (orderItem.quantity, itemCode)).list.map {
        case (quantity, itemCode) => <item code={itemCode} amount={quantity.toString} />
      } }
      </order>
    })
    Ok(<pendingOrders>{orders}</pendingOrders>)
  } } }

  def updateOrders() = Action(parse.xml) { implicit req => syncAllowed { database.withTransaction { implicit sess =>
    for (order <- req.body \\ "document") {
      CustomerOrders.orders.filter(_.id === asInt(order \ "id")).map(row => (row.postedNumber, row.status))
        .update((Some((order \ "number").text), DocumentStatus.Posted))
    }
    Ok
  } } }

  def updateOrgUnits() = Action(parse.xml) { implicit req => syncAllowed { database.withTransaction { implicit sess =>
    for (ouXml <- req.body \\ "orgUnit") {
      val name = (ouXml \ "name").text
      val posCountNode = (ouXml \ "posCount")
      val posCount = if(posCountNode.isEmpty) 0 else posCountNode.text.toInt
      logger.debug(s"Updating org unit $name")
      val ouId = OrgUnits.orgUnits.filter(_.name === name).map(_.id).firstOption match {
        case Some(id) =>
          OrgUnits.orgUnits.filter(_.id === id).map(_.posCount).update(posCount)
          id
        case None => (OrgUnits.orgUnits returning OrgUnits.orgUnits.map(_.id)) +=
          OrgUnits.OrgUnit(None, name, posCount)
      }
      Customers.customers.filter(_.customerId === ouId).delete
      for (supplierXml <- ouXml \ "supplier") {
        val supplierName = (supplierXml \ "name").text
        logger.info(s"as customer of $supplierName")
        val subGroup = supplierXml \ "subGroup"
        val rootGroupId = (supplierXml \ "catalogGroup").text.toInt
        val catalogGroupId = if (subGroup.isEmpty) rootGroupId
          else Items.itemGroups.filter(group => group.name === subGroup.text && group.parentId === rootGroupId).map(_.id)
              .firstOption.getOrElse(rootGroupId)
        Customers.customers.map(row => (row.customerId, row.supplierId, row.catalogGroupId)) insert OrgUnits.orgUnits.filter(_.name === supplierName)
          .map(supplier => (ouId, supplier.id, catalogGroupId))
      }
      Suppliers.supplierItems.filter(_.orgUnitId === ouId).delete
      for (supplierItemXml <- ouXml \ "supplierItem") {
        val itemCode = (supplierItemXml \ "item").text
        val priceStr = (supplierItemXml \ "price").text
        Suppliers.supplierItems map(row => (row.orgUnitId, row.itemId, row.price)) insert Items.items.filter(_.syncCode === itemCode)
          .map(item => (ouId, item.id, if (priceStr.isEmpty) BigDecimal(0) else BigDecimal(priceStr)))
      }
    }
    Ok
  } } }

  val pendingReceiptsQuery = for {
    receipt <- GoodsReceipts.goodsReceipts if receipt.status === DocumentStatus.ReadyToPost
    orgUnitName <- receipt.orgUnit.map(_.name)
    supplierName <- receipt.supplier.map(_.name)
  } yield (receipt, orgUnitName, supplierName)
  def receiptItemsQuery(id: Int) = for {
    receiptItem <- GoodsReceipts.goodsReceiptsItems if receiptItem.receiptId === id
    itemCode <- receiptItem.item.map(_.syncCode)
  } yield (itemCode, receiptItem.price, receiptItem.quantity)


  def getPendingGoodsReceipts() = Action { implicit req => syncAllowed { database.withSession { implicit sess =>
    Ok(<pendingGoodsReceipts> {
      pendingReceiptsQuery.list.map({
        case (receipt, ouName, supplierName) =>
          val rcptId = receipt.idOpt.get
          <goodsReceipt id={rcptId.toString} ou={ouName} supplier={supplierName} created={dtf.format(receipt.created.atZone(ZoneId.systemDefault()))}
                        wbNumber={receipt.waybillNumber.getOrElse("")} wbDate={receipt.waybillDate.map(df.format).getOrElse("")}
                        invNumber={receipt.invoiceNumber.getOrElse("")} invDate={receipt.invoiceDate.map(df.format).getOrElse("")}
                        comment={receipt.comment}>
            { receiptItemsQuery(rcptId).list.map {
            case (itemCode, price, quantity) => <item code={itemCode} price={price.toString} quantity={quantity.toString} />
          } }
          </goodsReceipt>
      })
    } </pendingGoodsReceipts>)
  } } }
  def updateGoodsReceipts() = Action(parse.xml) { implicit req => syncAllowed { database.withTransaction { implicit sess =>
    for (receipt <- req.body \\ "document") {
      GoodsReceipts.goodsReceipts.filter(_.id === asInt(receipt \ "id")).map(row => (row.postedNumber, row.status))
        .update((Some((receipt \ "number").text), DocumentStatus.Posted))
    }
    Ok
  } } }

  val pendingWriteoffsQuery = for {
    writeoff <- GoodsWriteoffs.goodsWriteoffs if writeoff.status === DocumentStatus.ReadyToPost
    orgUnitName <- writeoff.orgUnit.map(_.name)
  } yield (writeoff, orgUnitName)
  def writeoffItemsQuery(id: Int) = for {
    item <- GoodsWriteoffs.goodsWriteoffItems if item.writeoffId === id
    itemCode <- item.item.map(_.syncCode)
  } yield (itemCode, item.quantity)
  def getPendingGoodsWriteoffs() = Action { implicit req => syncAllowed { database.withSession { implicit sess =>
    Ok(<pendingGoodsWriteoffs> {
      pendingWriteoffsQuery.list.map({
        case (writeoff, ouName) =>
          val woId = writeoff.idOpt.get
          <goodsWriteoff id={woId.toString} ou={ouName} created={dtf.format(writeoff.created.atZone(ZoneId.systemDefault()))}
                        comment={writeoff.comment}>
            { writeoffItemsQuery(woId).list.map {
            case (itemCode, quantity) => <item code={itemCode} quantity={quantity.toString} />
          } }
          </goodsWriteoff>
      })
      } </pendingGoodsWriteoffs>)
  } } }
  def updateGoodsWriteoffs() = Action(parse.xml) { implicit req => syncAllowed { database.withTransaction { implicit sess =>
    for (receipt <- req.body \\ "document") {
      GoodsWriteoffs.goodsWriteoffs.filter(_.id === asInt(receipt \ "id")).map(row => (row.postedNumber, row.status))
        .update((Some((receipt \ "number").text), DocumentStatus.Posted))
    }
    Ok
  } } }

  def parseLocalDate(str: String) = df.parse(str, TemporalQueries.localDate())

  def updateRoyaltyBills() = Action(parse.xml) { implicit req => syncAllowed { database.withTransaction { implicit sess =>
    for (billXml <- req.body \\ "royaltyBill") OrgUnits.orgUnits
      .filter(_.name === (billXml \ "orgUnit").text).map(_.id).firstOption.foreach { orgUnitId =>
      val periodStart = parseLocalDate((billXml \ "periodStart").text)
      val periodEnd = parseLocalDate((billXml \ "periodEnd").text)
      val totalSales = BigDecimal((billXml \ "totalSales").text)
      val royaltySum = BigDecimal((billXml \ "royaltySum").text)
      val paid = BigDecimal((billXml \ "paid").text)
      RoyaltyBills.royaltyBills.filter( bill => bill.orgUnitId === orgUnitId && bill.periodStart === periodStart && bill.periodEnd === periodEnd)
      .map(_.id).firstOption match {
        case Some(billId) => RoyaltyBills.royaltyBills.filter(_.id === billId)
          .map(bill => (bill.totalSales, bill.royaltySum, bill.paid))
          .update(totalSales, royaltySum, paid)
        case None => RoyaltyBills.royaltyBills += RoyaltyBills.RoyaltyBill(None, orgUnitId, Instant.now(), periodStart, periodEnd, totalSales, royaltySum, paid)
      }
    }
    Ok
  } } }

  def readFile(f: Files.TemporaryFile) = {
    val byteBuffer = new Array[Byte](f.file.length().toInt)
    val is = new FileInputStream(f.file)
    is.read(byteBuffer)
    is.close()
    f.clean()
    byteBuffer
  }

  def updateRoyaltyForm(orgUnit:String, periodStart: String, periodEnd: String, formTypeStr: String) = Action(parse.temporaryFile) { implicit req => syncAllowed { database.withTransaction { implicit sess =>
    RoyaltyBills.royaltyBills.filter(bill =>
      bill.periodStart === parseLocalDate(periodStart) &&
      bill.periodEnd === parseLocalDate(periodEnd) &&
      bill.orgUnit.filter(_.name === orgUnit).exists
    ).map(_.id).firstOption.foreach { billId =>
      val formType = RoyaltyBills.FormType.withName(formTypeStr)
      RoyaltyBills.royaltyBillForms.filter(form => form.billId === billId && form.formType === formType).delete
      val byteBuffer = readFile(req.body)
      RoyaltyBills.royaltyBillForms += RoyaltyBillForm(billId, formType, byteBuffer)
    }
    Ok
  } } }

  def updateBills() = Action(parse.xml) { implicit req => syncAllowed { database.withTransaction { implicit sess =>
    for (billXml <- req.body \\ "bill") OrgUnits.orgUnits
      .filter(_.name === (billXml \ "orgUnit").text).map(_.id).firstOption.foreach { orgUnitId =>
      val date = parseLocalDate((billXml \ "date").text)
      val number = (billXml \ "number").text
      val orderXml = billXml \ "order"
      val orderId = if (orderXml.length > 0) {
        val orderNumber = (orderXml \ "number").text
        val orderDate = parseLocalDate((orderXml \ "date").text)
        val periodStart = Instant.from(orderDate.atTime(0,0).atZone(ZoneId.systemDefault()))
        val periodEnd = Instant.from(orderDate.atTime(23,59,59).atZone(ZoneId.systemDefault()))
        CustomerOrders.orders.filter(o => o.supplierId === orgUnitId && o.created.between(periodStart, periodEnd) &&
          o.postedNumber === orderNumber).map(_.id).firstOption
      } else None
      val supplierId = OrgUnits.orgUnits.filter(_.name === (billXml \ "supplier").text).map(_.id).first
      val sum = BigDecimal((billXml \ "sum").text)
      val paid = BigDecimal((billXml \ "paid").text)
      val comment = (billXml \ "comment").text
      Bills.bills.filter(bill => bill.orgUnitId === orgUnitId && bill.date === date && bill.number === number).map(_.id)
      .firstOption match {
        case Some(id) => Bills.bills.filter(_.id === id).map(bill => (bill.supplierId, bill.sum, bill.paid, bill.orderId.?, bill.comment))
          .update((supplierId, sum, paid, orderId, comment))
        case None => Bills.bills += Bills.Bill(None, date, number, orgUnitId, supplierId, sum, paid, orderId, comment)
      }
    }
    Ok
  } } }

  def updateBillForm(orgUnit: String, date: String, number: String) = Action(parse.temporaryFile) { implicit req => syncAllowed { database.withTransaction { implicit sess =>
    Bills.bills.filter(bill =>
      bill.date === parseLocalDate(date) &&
      bill.number === number &&
      bill.orgUnit.filter(_.name === orgUnit).exists
    ).map(_.id).firstOption.foreach { billId =>
      Bills.billForms.filter(_.billId === billId).delete
      val byteBuffer = readFile(req.body)
      Bills.billForms += (billId, byteBuffer)
    }
    Ok
  } } }

  def updateReportingPeriods = Action(parse.xml) { implicit req => syncAllowed { database.withTransaction { implicit sess =>
    for (periodXml <- req.body \\ "reportingPeriod") {
      val year = (periodXml \ "year").text.toInt
      val number = (periodXml \ "number").text.toInt
      val start = parseLocalDate((periodXml \ "start").text)
      val end = parseLocalDate((periodXml \ "end").text)
      ReportingPeriods.reportingPeriods.filter(rp => rp.year === year && rp.number === number).delete
      ReportingPeriods.reportingPeriods += ReportingPeriod(number, year, start, end)
    }
    Ok
  } } }

  def updateIncomeReports = Action(parse.xml) { implicit req => syncAllowed { database.withTransaction { implicit sess =>
    for (incomeReport <- req.body \\ "incomeReport") {
      val orgUnitId = OrgUnits.orgUnits.filter(_.name === (incomeReport \ "orgUnit").text).map(_.id).first
      val date = parseLocalDate((incomeReport \ "date").text)
      val posNumber = (incomeReport \ "posNumber").text.toInt
      val income = BigDecimal((incomeReport \ "income").text)
      val count = (incomeReport \ "count").text.toInt
      IncomeReports.incomeReports.filter(r => r.orgUnitId === orgUnitId && r.date === date && r.posNumber === posNumber).delete
      IncomeReports.incomeReports += IncomeReport(orgUnitId, date, posNumber, income, count)
    }
    Ok
  } } }

  def getIncomeReports(from: String, to: String) = Action { implicit req => syncAllowed { database.withTransaction { implicit sess =>
    val reports = (for {
      report <- IncomeReports.incomeReports if report.date.between(parseLocalDate(from), parseLocalDate(to))
      orgUnit <- report.orgUnit
    } yield (report, orgUnit.name))
      .mapResult({
      case (report, ouName) => <incomeReport ou={ouName} date={df.format(report.date)} posNumber={report.posNumber.toString}
                                             totalIncome={report.totalIncome.toString} totalCount={report.totalCount.toString}/>
    }).list
    logger.info(s"Requested income reports from $from to $to, returning ${reports.size} entries")
    Ok(<incomeReports>
      {for (reportXml <- reports) yield reportXml}
    </incomeReports>)
  } } }

  def syncForm = Action {
    if (Play.isDev) Ok(views.html.sync()) else Forbidden
  }

}
