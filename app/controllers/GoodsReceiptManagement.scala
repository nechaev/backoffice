package controllers

import java.time.{LocalDate, Instant}
import model.Role.Warehouse
import model._
import tools.DbProfile._
import dbProfile.simple._
import model.GoodsReceipts._
import play.api.data.Form
import play.api.data.Forms._
import play.api.libs.json.Json
import model.DocumentStatus.DocumentStatus
import model.GoodsReceipts.GoodsReceipt
import scala.Some
import model.GoodsReceipts.GoodsReceiptItem

/**
 * Created by vnechaev on 10.05.2014.
 */
object GoodsReceiptManagement extends AbstractController {
  case class ReceiptListItem(id: Int, created: Instant, orgUnit: String, supplier:String, sum: BigDecimal, status: DocumentStatus, number: Option[String])

  def allowedReceiptById(receiptId: Int)(implicit ctx: AuthRequestContext) = {
    goodsReceipts.filter(r => r.id === receiptId && orgUnitAllowed(r.orgUnitId))
  }
  def editableReceiptById(receiptId: Int)(implicit ctx: AuthRequestContext) = {
    allowedReceiptById(receiptId).filter(_.status === DocumentStatus.Draft)
  }
  def list = authenticated(Warehouse) { implicit ctx => database.withSession { implicit sess =>
    val listItems = (for {
      rcpt <- goodsReceipts.sortBy(_.created.desc) if orgUnitAllowed(rcpt.orgUnitId)
      orgUnit <- rcpt.orgUnit.map(_.name)
      supplier <- rcpt.supplier.map(_.name)
    } yield (rcpt.id, rcpt.created, orgUnit, supplier, rcpt.sum, rcpt.status, rcpt.postedNumber))
      .take(maxListResultLimit).mapResult(ReceiptListItem.tupled).list
    Ok(views.html.goodsReceiptsList(listItems))
  } }

  def supplierItemsQuery(supplierId: Int) = for {
    si <- Suppliers.supplierItems.filter(_.orgUnitId === supplierId)
    item <- si.item
  } yield (item, si.itemId, si.price)

  private def receiptEditForm(goodsReceipt: GoodsReceipt, items: Map[Int, GoodsReceiptItem])(implicit ctx: AuthRequestContext, sess: Session) = {
    val orgUnits = OrgUnits.orgUnitsForUser(ctx.user.id).list
    val suppliers = OrgUnits.orgUnits.filter(ou => Suppliers.supplierItems.filter(_.orgUnitId === ou.id).exists).list
    val supplierItems = supplierItemsQuery(goodsReceipt.supplierId).list
    val priceMap = Map(supplierItems.map({
      case (_, itemId, price) => (itemId, price)
    }): _*)
    val itemsMap = items.withDefault(itemId => GoodsReceiptItem(0, itemId, priceMap(itemId), 0))
    Ok(views.html.goodsReceipt(goodsReceipt, itemsMap, orgUnits, suppliers, supplierItems))
  }

  def newReceipt = authenticated(Warehouse) { implicit ctx => database.withSession { implicit sess =>
    (for {
      defaultOuId <- OrgUnits.orgUnitsForUser(ctx.user.id).map(_.id).firstOption
      defaultSupplierId <- Suppliers.supplierItems.map(_.orgUnitId).firstOption
    } yield {
      val emptyReceipt = GoodsReceipt(None, Instant.now, defaultOuId, defaultSupplierId, 0, None, None, None, None, "", DocumentStatus.Draft, None)
      receiptEditForm(emptyReceipt, Map())
    }).getOrElse(Redirect(routes.GoodsReceiptManagement.list).withNotification("Создание нового поступления невозможно", NotificationLevel.Error))
  } }

  def editReceipt(id: Int) = authenticated(Warehouse) { implicit ctx => database.withSession { implicit sess =>
    allowedReceiptById(id).firstOption match {
      case Some(receipt) => receiptEditForm(GoodsReceipts(id).first,
        GoodsReceipts.goodsReceiptsItems.filter(_.receiptId === id).list.groupBy(_.itemId).mapValues(_.head))
      case None => Redirect(routes.GoodsReceiptManagement.list).withNotification("Выбранный документ недоступен")
    }
  } }

  def receiptItemsTable(supplierId: Int) = authenticated(Warehouse) { implicit ctx => database.withSession { implicit sess =>
    val supplierItems = supplierItemsQuery(supplierId).sortBy(_._1.name).list
    val priceMap = Map(supplierItems.map({
      case (_, itemId, price) => (itemId, price)
    }): _*)
    val itemsMap = Map[Int, GoodsReceiptItem]().withDefault(itemId => GoodsReceiptItem(0, itemId, priceMap(itemId), 0))
    Ok(views.html.goodsReceiptItems(supplierItems, itemsMap))
  } }

  val receiptForm = Form(tuple(
    "id" -> optional(number),
    "orgUnit" -> number,
    "supplier" -> number,
    "comment" -> text,
    "waybillNumber" -> text,
    "waybillDate" -> text,
    "invoiceNumber" -> text,
    "invoiceDate" -> text,
    "receiptItems" -> text,
    "commit" -> text
  ) )

  case class ReceiptItemInput(itemId: Int, price: BigDecimal, quantity: BigDecimal)

  def saveReceipt = authenticated(Warehouse) { implicit ctx => database.withTransaction { implicit sess =>
    implicit val itemInputReads = Json.reads[ReceiptItemInput]
    import ctx.req
    receiptForm.bindFromRequest().fold(
      withErrors => BadRequest(withErrors.errorsAsJson), {
      case (idOpt, orgUnit, supplier, comment, wbNumber, wbDate, invNumber, invDate, itemsJson, commit) =>
        val items = Json.parse(itemsJson).as[List[ReceiptItemInput]]
        val sum = items.map(item => item.price * item.quantity).reduce(_ + _)
        val receiptId = idOpt match {
          case Some(rcptId) =>
            val updated = editableReceiptById(rcptId)
            .map(row => (row.orgUnitId, row.supplierId, row.comment, row.waybillNumber, row.waybillDate, row.invoiceNumber, row.invoiceDate))
            .update((orgUnit, supplier, comment, strOpt(wbNumber), dateOpt(wbDate), strOpt(invNumber), dateOpt(invDate)))
            if (updated > 0) {
              goodsReceiptsItems.filter(_.receiptId === rcptId).delete
              Some(rcptId)
            } else None
          case None =>
            val newId = (GoodsReceipts.goodsReceipts returning GoodsReceipts.goodsReceipts.map(_.id)) += GoodsReceipt(None, Instant.now(), orgUnit, supplier, sum,
              strOpt(wbNumber), dateOpt(wbDate), strOpt(invNumber), dateOpt(invDate), comment, DocumentStatus.Draft, None)
            Some(newId)
        }
        val redirect = Redirect(routes.GoodsReceiptManagement.list)
        receiptId match {
          case Some(id) =>
            goodsReceiptsItems ++= items.map(item => GoodsReceiptItem(id, item.itemId, item.price, item.quantity))
            if (commit equals "true") goodsReceipts.filter(_.id === id).map(_.status).update(DocumentStatus.ReadyToPost)
            redirect.withNotification("Документ сохранен", NotificationLevel.Success)
          case None => redirect.withNotification("Документ не может быть сохранен", NotificationLevel.Error)
        }
    })
  } }
  def delete(id: Int) = authenticated(Warehouse) { implicit ctx => database.withTransaction { implicit sess =>
    val deleted = editableReceiptById(id).delete
    val redirect = Redirect(routes.GoodsWriteoffManagement.list)
    if (deleted > 0) redirect.withNotification("Документ удален", NotificationLevel.Success)
    else redirect.withNotification("Документ не может быть удален", NotificationLevel.Error)
  } }

}
