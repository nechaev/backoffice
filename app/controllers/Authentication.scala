package controllers

import model.Users
import play.api.data.Forms._
import play.api.data._
import play.api.data.format.Formats._
import play.api.mvc.Action
import tools.DbProfile._
import dbProfile.simple._

object Authentication extends AbstractController {

  def index = authenticated { implicit ctx =>
    Ok(views.html.index())
  }

  def login = action { implicit ctx =>
    Ok(views.html.login())
  }


  private val loginForm = Form(tuple(
    "login" -> of[String],
    "password" -> of[String]
  ))

  def userIdQuery(login: String, password: String) = for {
    passwordRow <- Users.passwords if passwordRow.password === password
    user <- passwordRow.user if user.login === login
  } yield {
    user.id
  }

  def doLogin() = Action { implicit req =>
    val (login, password) = loginForm.bindFromRequest().get
    database.withSession { implicit sess =>
      userIdQuery(login, password).firstOption match {
        case Some(userId) => Redirect(routes.Authentication.index()).withSession("userId" -> userId.toString)
        case None => Redirect(routes.Authentication.login())
          .withNotification("Вход не выполнен - возможно логин и/или пароль был указан неверно", NotificationLevel.Error)
      }
    }
  }
  def logout = Action { implicit req =>
    Redirect(routes.Authentication.index()).withNewSession
  }

}