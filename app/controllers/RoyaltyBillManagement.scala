package controllers

import java.time.LocalDate
import model.Role.Franchise
import tools.DbProfile._
import dbProfile.simple._
import model.RoyaltyBills.{FormType, royaltyBills, royaltyBillForms}
import model.OrgUnits.userOrgUnitsForUser
import play.api.mvc.{ResponseHeader, Result}
import play.api.libs.iteratee.Enumerator
import scala.language.postfixOps

/**
 * Created by vnechaev on 13.05.2014.
 */
object RoyaltyBillManagement extends AbstractController {
  case class RoyaltyBillListItem(id: Int, orgUnit: String, start: LocalDate, end: LocalDate, totalSales: BigDecimal, royaltySum: BigDecimal, paid: BigDecimal)
  def list = authenticated(Franchise) { implicit ctx => database.withSession { implicit sess =>
    val bills = (for {
      bill <- royaltyBills.sortBy(_.periodStart desc) if userOrgUnitsForUser(ctx.user.id).filter(_.orgUnitId === bill.orgUnitId).exists
      orgUnit <- bill.orgUnit.map(_.name)
    } yield (bill.id, orgUnit, bill.periodStart, bill.periodEnd, bill.totalSales, bill.royaltySum, bill.paid)).mapResult(RoyaltyBillListItem.tupled).list
    Ok(views.html.royaltyBills(bills))
  } }

  def billForm(billId: Int, formType: String) = authenticated(Franchise) { implicit ctx => database.withSession { implicit sess =>
    royaltyBillForms.filter(form => form.billId === billId &&
      form.formType === FormType.withName(formType) &&
      form.bill.filter(b => orgUnitAllowed(b.orgUnitId)).exists
    ).map(_.formData).firstOption match {
      case Some(bytes) => Result(
        header = ResponseHeader(200, Map(CONTENT_TYPE -> "application/pdf")),
        body = Enumerator(bytes)
      )
      case None => Redirect(routes.RoyaltyBillManagement.list()).withNotification("Выбранная форма отсутствует", NotificationLevel.Error)
    }
  } }

}
