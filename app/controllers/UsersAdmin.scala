package controllers

import model.{Role, OrgUnits, User, Users}
import Users.users
import model.Role.Admin
import play.api.data.Form
import play.api.data.Forms._
import tools.DbProfile._
import dbProfile.simple._
import com.typesafe.scalalogging.LazyLogging

/**
 * Created by sgl on 14.04.14.
 */
object UsersAdmin extends AbstractController with LazyLogging {

  def usersList = authenticated(Admin) {
    implicit ctx =>
      Ok(views.html.users(database.withSession {
        implicit session =>
          users.sortBy(_.name).list
      }))
  }

  def availableOrgUnits(implicit sess: Session) = OrgUnits.orgUnits.sortBy(_.name).list

  def user(userId: Int) = authenticated(Admin) {
    implicit ctx => database.withSession {
      implicit sess =>
        Users(userId).firstOption match {
          case Some(user) =>
            val userOrgUnits = Set(OrgUnits.userOrgUnitsForUser(user.id).map(_.orgUnitId).list: _*)
            val userRoles: Set[Role] = Set(Users.userRoles(user.id).list: _*)
            Ok(views.html.user(user, userOrgUnits, userRoles, availableOrgUnits))
          case None => NotFound
        }
    }
  }

  def newUser = authenticated(Admin) {
    implicit ctx => database.withSession {
      implicit sess =>
        Ok(views.html.user(User(None, "", ""), Set(), Set(), availableOrgUnits))
    }
  }

  def updateUser() = authenticated(Admin) {
    implicit ctx =>
      ctx.req.body.asFormUrlEncoded match {
        case Some(userForm) => {
          val idOpt = userForm.get("id").map(_.head.toInt)
          val name = userForm("name").head
          val login = userForm("login").head
          val roles = userForm("roles").head
          val orgs = userForm.get("org_units").map(_.map(_.toInt)).getOrElse(Nil)

          logger.info("updateUser: id {}, login {}", idOpt, login)
          database.withTransaction {
            implicit sess =>
              val loginQuery = idOpt match {
                case Some(id) => users.filter(u => u.login === login && u.id =!= id)
                case None => users.filter(_.login === login)
              }
              val loginExists = Query(loginQuery.exists).first
              if (loginExists) {
                Redirect(idOpt.map(routes.UsersAdmin.user).getOrElse(routes.UsersAdmin.newUser()))
                  .withNotification("Этот логин уже занят", NotificationLevel.Error)
              } else {
                val userId: Int = idOpt match {
                  case Some(id) => {
                    Users(id).map(row => (row.name, row.login))
                      .update((name, login))
                    id
                  }
                  case None => (users returning users.map(_.id)) += User(None, login, name)
                }

                Users.roles.filter(_.userId === userId).delete
                if (!roles.isEmpty) Users.roles ++= roles.split(',').map(Role.apply).map(role => (userId, role.id))

                OrgUnits.userOrgUnitsForUser(userId).delete
                if (!orgs.isEmpty) OrgUnits.userOrgUnits ++= orgs.map((userId, _))
                Redirect(routes.UsersAdmin.user(userId))
                  .withNotification("Данные пользователя успешно сохранены", NotificationLevel.Success)
              }
          }
        }
        case _ => BadRequest
      }
  }

  private val passwordForm = Form(tuple(
    "id" -> number, "password" -> text
  ))

  def setPassword() = authenticated(Admin) { implicit ctx => database.withTransaction { implicit sess =>
      import ctx.req
      val (id, password) = passwordForm.bindFromRequest().get
      logger.info(s"setPassword: $id")
      Users.passwords.filter(_.userId === id).delete
      Users.passwords += (id, password)
      Redirect(routes.UsersAdmin.user(id)).withNotification("Пароль успешно изменен", NotificationLevel.Success)
  } }

}

case class UserListItem(id: Int, name: String, login: String)