name := "backoffice"

scalaVersion := "2.11.2"

version := "1.1"

libraryDependencies ++= Seq(
  jdbc,
  "com.typesafe.slick" %% "slick" % "2.1.0",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0"
)     

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalacOptions ++= Seq("-feature", "-deprecation")