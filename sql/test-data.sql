delete from users;
alter sequence users_id_seq restart;
insert into users(login, name) values ('test@test', 'John Doe');

delete from items;
alter sequence items_id_seq restart;
insert into items(name, uom, weight, volume) values ('Рога', 'шт', 1.0, 0.2);
insert into items(name, uom, weight, volume) values ('Копыта', 'шт', 0.5, 0.05);

delete from item_groups;
alter sequence item_groups_id_seq restart;
insert into item_groups(name) values ('Каталог номенклатуры');
insert into item_groups(name, parent) values ('Товары', 1);

delete from item_group_items;
INSERT INTO item_group_items (groupid, itemid) VALUES (2, 1);
INSERT INTO item_group_items (groupid, itemid) VALUES (2, 2);

delete from org_units;
alter sequence org_units_id_seq restart;
insert into org_units(name) values ('HQ');
insert into org_units(name) values ('Рога&Копыта');
insert into org_units(name) values ('ИП Петрович');

delete from customers;
insert into customers(customer, supplier, catalogitemgroup) values (2, 1, 1);
insert into customers(customer, supplier, catalogitemgroup) values (2, 3, 1);

delete from order_items;
delete from orders;
alter sequence orders_id_seq restart;
insert into orders(author, customer, supplier, created, posted, number)
  values (1, 2, 1, '2014-01-01 12:00:00', null, null);
insert into orders(author, customer, supplier, created, posted, number)
  values (1, 2, 1, '2014-01-02 14:00:00', '2014-01-02 15:30:00', '123');
insert into order_items(orderid, itemid, quantity) values (1, 1, 2.0);
insert into order_items(orderid, itemid, quantity) values (1, 2, 3.0);
insert into order_items(orderid, itemid, quantity) values (2, 1, 4.0);
insert into order_items(orderid, itemid, quantity) values (2, 2, 5.0);
