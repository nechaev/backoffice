CREATE TABLE users (
  id    SERIAL PRIMARY KEY,
  login VARCHAR(30) UNIQUE NOT NULL,
  name  VARCHAR(50)        NOT NULL
);

CREATE TABLE roles (
  userId INT         NOT NULL REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE,
  roleId VARCHAR(10) NOT NULL,
  UNIQUE (userId, roleId)
);

CREATE TABLE passwords (
  userId   INT NOT NULL REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE,
  password VARCHAR(30)
);

CREATE TABLE org_units (
  id   SERIAL PRIMARY KEY,
  name VARCHAR(100)
);

CREATE TABLE org_users (
  userId INT NOT NULL REFERENCES users (id) ON DELETE CASCADE ON UPDATE CASCADE,
  ou     INT NOT NULL REFERENCES org_units (id)
);

CREATE TABLE items (
  id     SERIAL PRIMARY KEY,
  name   VARCHAR(100),
  uom    VARCHAR(30),
  weight NUMERIC(10, 3),
  volume NUMERIC(10, 3),
  price  NUMERIC(10, 2)
);

CREATE TABLE item_groups (
  id     SERIAL PRIMARY KEY,
  parent INT REFERENCES item_groups (id),
  name   VARCHAR(100) NOT NULL
);

CREATE TABLE item_group_items (
  groupId INT NOT NULL REFERENCES item_groups (id) ON DELETE CASCADE ON UPDATE CASCADE,
  itemId  INT NOT NULL REFERENCES items (id)
);
CREATE INDEX item_group_items_idx ON item_group_items (groupId);

CREATE TABLE orders (
  id       SERIAL PRIMARY KEY,
  author   INT          NOT NULL REFERENCES users,
  customer INT          NOT NULL REFERENCES org_units,
  supplier INT          NOT NULL REFERENCES org_units,
  created  TIMESTAMP    NOT NULL,
  posted   TIMESTAMP,
  number   VARCHAR(10),
  comment  VARCHAR(150) NOT NULL DEFAULT ''
);

CREATE TABLE order_items (
  orderId  INT NOT NULL REFERENCES orders (id) ON DELETE CASCADE ON UPDATE CASCADE,
  itemId   INT NOT NULL REFERENCES items (id),
  quantity NUMERIC(14, 3)
);
CREATE INDEX order_items_idx ON order_items (orderId);

CREATE TABLE customers (
  customer         INT NOT NULL REFERENCES org_units (id) ON DELETE CASCADE ON UPDATE CASCADE,
  supplier         INT NOT NULL REFERENCES org_units (id),
  catalogItemGroup INT NOT NULL REFERENCES item_groups (id),
  UNIQUE (customer, supplier)
);
