function parseNumber ( v ) {
    if ( v === undefined || v == "" ) return 0.0 ;
    var result = parseFloat ( v.replace ( ",", "." ) ) ;
    if ( isNaN ( result ) ) result = 0.0 ;
    return result ;
}
function formatNumber ( v, frac, showZero) {
    if (!v) v = 0.0;
    if ( v == 0.0 && !showZero) return "";
    else return v.toFixed ( frac ).replace ( ".", "," ) ;
}
function metaValue(name) {
    return $('meta[name="' + name + '"]').attr('content');
}