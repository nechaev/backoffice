$ ( function ( ) {
    function updateItemInput ( ) {
        var itemsInputVal =[ ] ;
        $ ( '#receiptItemsTable' ).find ( 'tr' ).each ( function ( num, tr ) {
            var row = $ ( tr ) ;
            var itemId = row.attr ( "data-item-id" ) ;
            if ( itemId ) {
                var priceInput = row.find ( '.input-price' ) ;
                var quantInput = row.find ( '.input-quantity' ) ;
                var sumCell = row.find ( '.sum-cell' ) ;
                var quantity = parseNumber ( quantInput.val ( ) ) ;
                if ( quantity == 0.0 ) {
                    delete itemsInputVal[ itemId ] ;
                    quantInput.val ( '' ) ;
                    sumCell.html ( '&nbsp' ) ;
                } else {
                    var itemLine = {
                        itemId : parseInt ( itemId ),
                        price : parseNumber ( priceInput.val ( ) ),
                        quantity : quantity
                    } ;
                    itemsInputVal.push ( itemLine ) ;
                    priceInput.val ( formatNumber ( itemLine.price, 2 ) ) ;
                    quantInput.val ( formatNumber ( itemLine.quantity, 3 ) ) ;
                    sumCell.html ( formatNumber ( itemLine.price * itemLine.quantity, 2 ) ) ;
                }
            }
        } ) ;
        var totalSum = itemsInputVal.reduce ( function ( acc, item ) { return acc + item.price * item.quantity ; }, 0.0 ) ;
        $ ( '#totalSum' ).html ( formatNumber ( totalSum, 2 ) ) ;
        $ ( '#receiptItems' ).val ( JSON.stringify ( itemsInputVal ) ) ;
    }
    updateItemInput ( ) ;
    $ ( '#receiptItemsTable' ).change ( updateItemInput ) ;
    var itemsUrl = $ ( 'meta[name="itemUrl"]' ).attr ( "content" ) ;
    $ ( '#supplier' ).change ( function ( ) {
        $ ( '#receiptItems' ).val ( '{}' ) ;
        var supplierId = $ ( '#supplier' ).val ( ) ;
        $ ( '#receiptItemsTable' ).load ( itemsUrl + supplierId, updateItemInput ) ;
    } ) ;
    $('#commitButton').click(function() {
        $('#commit').val('true');
        $('form').submit();
    });
} ) ;