$(function() {
    var updateUrl = metaValue('updateUrl');
    $('select').change(function() {
        $('form').submit();
    });
    $('input').change(function(event) {
        var input = $(event.target);
        var row = input.parents('tr');
        var posNumber = parseInt(input.attr('data-pos-number'));
        var request = {
            orgUnitId: $('#orgUnitId').val(),
            date: input.attr('data-row-date'),
            posNumber: posNumber,
            sum: parseNumber(row.find('input.sum[data-pos-number=' + posNumber + ']').val()),
            count: parseNumber(row.find('input.count[data-pos-number=' + posNumber + ']').val())
        };
        $.post(updateUrl, request, function(result) {
            row.find('input.sum[data-pos-number=' + posNumber + ']').val(formatNumber(request.sum, 2, true));
            row.find('input.count[data-pos-number=' + posNumber + ']').val(formatNumber(request.count, 0, true));
        });
    });
});
