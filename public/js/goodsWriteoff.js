$(function() {
    var itemListUrl = $('meta[name="itemListUrl"]').attr("content");
    var newLineUrl = $('meta[name="newLineUrl"]').attr("content");
    function updateWriteoffItems() {
        var items = [];
        $('input.input-quantity').each(function() {
            var $this = $(this);
            var itemId = parseInt($this.parents('tr').attr('data-item-id'));
            var quantity = parseNumber($this.val());
            items.push({item: itemId, quantity: quantity});
            $this.val(formatNumber(quantity, 3));
        });
        $('#writeoffItems').val(JSON.stringify(items));
    }
    $('#newItemInput').select2({
        minimumInputLength: 3,
        id: function(item) {
            return '' + item.idOpt;
        },
        formatResult: function(item) {
            return item.name + ' (' + item.uom + ')';
        },
        formatSelection: function(item) {
            return item.name;
        },
        query: function(query) {
            $.get(itemListUrl + query.term, undefined, function(data) {
                query.callback({
                    results: data
                });
            }, 'json');
        }
    }).on('change', function(e) {
        if ($('tr[data-item-id="' + e.val + '"]').length == 0) $.get(newLineUrl + e.val, undefined, function(data) {
            $('#newItemTableRow').before(data);
            updateWriteoffItems();
        }, 'html');
        $('#newItemInput').select2("val", "");
        e.stopPropagation();
    });
    var table = $('table');
    table.on('click', 'span.remove-item-action', function(e) {
        $(e.target).parents('tr').remove();
        updateWriteoffItems();
    });
    table.change(updateWriteoffItems);
    updateWriteoffItems();

    $('#commitButton').click(function() {
        $('#commit').val('true');
        $('form').submit();
    });
});