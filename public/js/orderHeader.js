$(function() {
    function updateAvailability() {
        var selCustomer = $('#customer' ).val();
        var supplierIds = suppliersByCustomers[selCustomer];
        $('#supplier > option' ).each(function(idx, nodeEl) {
            var node = $(nodeEl);
            if (supplierIds.indexOf(node.attr('value')) >= 0) node.removeAttr('disabled');
            else node.attr('disabled','disabled');
        });
        $('#supplier' ).val(supplierIds[0]);
    }
    updateAvailability();
    $('#customer' ).change(updateAvailability);
});
