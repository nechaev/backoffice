$(function() {
    var updateAmountUrl = metaValue('updateAmountUrl');
    var orderId = metaValue('orderId');

    function updateAmount(newVal, inputEl) {
        var params = {
            id: orderId,
            item: inputEl.attr('data-item-id'),
            qty: newVal
        };
        $.post(updateAmountUrl, params, function(result) {
            inputEl.val(formatNumber(newVal, 3, true));
            $('#totalCell').html('<h4>' + formatNumber(result, 2, true) + '</h4>')
        }, 'json');
    }
    function getAmount(inputEl) {
        return parseFloat(inputEl.val());
    }
    $('button.item-amt-up').click(function(evt) {
        var inputEl = $('input.item-amt', evt.target.parentNode.parentNode);
        updateAmount(getAmount(inputEl) + 1, inputEl);
    });
    $('button.item-amt-dn').click(function(evt) {
        var inputEl = $('input.item-amt', evt.target.parentNode.parentNode);
        updateAmount(Math.max(getAmount(inputEl) - 1, 0), inputEl);
    });
    $('input.item-amt').change(function(evt) {
        var inputEl = $(evt.target);
        updateAmount(getAmount(inputEl), inputEl);
    });
});
